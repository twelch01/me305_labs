# -*- coding: utf-8 -*-
"""
@file main_lab3.py
@brief A main driver
@details This file is used to instantiate a driver object, two encoder class objects, and two motor objects, which are passed into Task_user Class in task_user.py.
@details The user has the power to keep running the program created in task_user.py and to quit whenever he/she wants. The number of times the program has been run will be displayed.

File link:  https://bitbucket.org/twelch01/me305_labs/src/master/Lab%203/

@image html Lab3data.JPG "Test Results" width=600px

@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""
import DRV8847
import task_encoder_file
import task_user
import motor_task
import share
if __name__ == '__main__':

    ## @brief A class object that represents encoder position 1
    #  @details This class object is instantiated by Share class to store encoder position 1
    position_1 = share.Share()
    ## @brief A class object that represents encoder 1 velocity 
    #  @details This class object is instantiated by Share class to store encoder 1 velocity 
    delta_1 = share.Share()
    ## @brief A class object that helps set encoder postion 1 to 0
    #  @details This class object is instantiated by Share class to store True or False to set encoder postion 1 to 0.
    setZero_1 = share.Share()
    ## @brief A class object that represents duty cycle for motor 1
    #  @details This class object is instantiated by Share class to store duty cycle which helps set encoder 1 velocity
    duty_1 = share.Share()
    ## @brief A list object that continas data for motor 1 and encoder 1
    #  @details This list object is used to store all the class objects that represent data for motor 1 and encoder 1.
    list_1 = [1, position_1, delta_1, setZero_1, duty_1]

    ## @brief A class object that represents encoder position 2
    #  @details This class object is instantiated by Share class to store encoder position 2
    position_2 = share.Share()
    ## @brief A class object that represents encoder 2 velocity 
    #  @details This class object is instantiated by Share class to store encoder 2 velocity 
    delta_2 = share.Share()
    ## @brief A class object that helps set encoder postion 2 to 0
    #  @details This class object is instantiated by Share class to store True or False to set encoder postion 2 to 0.
    setZero_2 = share.Share()
    ## @brief A class object that represents duty cycle for motor 2
    #  @details This class object is instantiated by Share class to store duty cycle which helps set encoder 2 velocity
    duty_2 = share.Share()

    ## @brief A list object that continas data for motor 2 and encoder 2
    #  @details This list object is used to store all the class objects that represent data for motor 2 and encoder 2.
    list_2 = [2, position_2, delta_2, setZero_2, duty_2]
    ## @brief A class object that represents data for motor 1 and encoder 1
    #  @details This class object is instantiated by Queue class to store data for encoder 1 and motor 1
    motor1_data = share.Queue()
    ## @brief A class object that represents data for motor 2 and encoder 2
    #  @details This class object is instantiated by Queue class to store data for encoder 2 and motor 2
    motor2_data = share.Queue()
    for i in range(len(list_1)):
        motor1_data.put(list_1[i])
        motor2_data.put(list_2[i])
    # Lab2

    ## @brief A class object that represents Task_encoder_file class object for encoder 1
    #  @details There are five parameters passed into this Task_encoderfile Class object: period and pin number, encoder position 1, encoder velocity 1, zero-encoder 1-position call.
    #  @details The period is defined as 10 miliseconds, and pin number is defined as 8.
    task_encoder_1 = task_encoder_file.Task_encoder(10, 8, position_1, delta_1, setZero_1)

    ## @brief A class object that represents Task_encoder_file class object for encoder 2
    #  @details There are five parameters passed into this Task_encoderfile Class object: period and pin number, encoder position 2, encoder velocity 2, zero-encoder 2-position call.
    #  @details The period is defined as 10 miliseconds, and pin number is defined as 4.
    task_encoder_2 = task_encoder_file.Task_encoder(10, 4, position_2, delta_2, setZero_2)

    # Lab3
    
    ## @brief A motor driver class object 
    #  @details The driver class is used to implement fault detection, fault condition clear, and instantiation of motor class object and to enable/disable motor.
    motor_drv = DRV8847.DRV8847()
    motor_drv.enable()
    
    ## @brief A class object that represents duty cycle for motor 2
    #  @details This class object is instantiated by Share class to store duty cycle which helps set encoder 2 velocity
    enable = share.Share()
    
    ## @brief A class object that represents Motor class object for motor 1
    #  @details There are five parameters passed into this Motor Class object: motor driver time, two pin number, two driver pyb pin objects.
    motor_1 = motor_drv.motor(motor_drv.tim, 3, 4, motor_drv.IN_1, motor_drv.IN_2)
    ## @brief A class object that represents Motor class object for motor 1
    #  @details There are five parameters passed into this Motor Class object: motor driver time, two pin number, two driver pyb pin objects.
    motor_2 = motor_drv.motor(motor_drv.tim, 1, 2, motor_drv.IN_3, motor_drv.IN_4)
    
    ## @brief A class object that represents Task_motor class object for motor 1
    #  @details There are five parameters passed into this Task_motor Class object: Motor 1 class object, motor driver class object, enable object.
    motor_task_1 = motor_task.Task_motor(motor_1, motor_drv, enable)
    ## @brief A class object that represents Task_motor class object for motor 2
    #  @details There are three parameters passed into this Task_motor Class object: Motor 2 class object, motor driver class object, enable object.
    motor_task_2 = motor_task.Task_motor(motor_2, motor_drv, enable)


    
    ## @brief A class object that represents Task_user class object 
    #  @details There are 4 parameters passed into this Task_user class object: a period that determines how frequent the data is collected, two motor data class objects, enable object
    task_user = task_user.Task_user(100, motor1_data,motor2_data, enable)
    print(' --------------------------------------------------------------------------------')
    print('| Press one of the following letters to run a command on Encoder:                |')
    print('|"z"- Zero the position of the encoder 1                                         |')
    print('|"Z"- Zero the position of the encoder 2                                         |')
    print('|"p"- print out the position of the encoder 1                                    |')
    print('|"P"- print out the position of the encoder 2                                    |')
    print('|"d"- print out the delta for encoder 1                                          |')
    print('|"D"- print out the delta for encoder 2                                          |')
    print('|"m"- Input Motor 1 Duty                                                         |')
    print('|"M"- Input Motor 2 Duty                                                         |')
    print('|"g"- Collect encoder data for 30 seconds and print data for Encoder 1           |')
    print('|"G"- Collect encoder data for 30 seconds and print data for Encoder 2           |')
    print('|"c or C"- Clear a fault condition triggered by the DRV8847 for Encoder 1 and 2  |')
    print('|"s"- end data collection prematurely for Encoder 1                              |')
    print('|"S"- end data collection prematurely for Encoder 2                              |')
    print('|"e or E"- end the program                                                       |')
    print(' -------------------------------------------------------------------------------')


    while(True):  # Unless User preqsses 'e', the user will keep running the program.
        task_user.run()
        task_encoder_1.run()
        task_encoder_2.run()
        
        motor_task_1.run_motor(duty_1.get())

        motor_task_2.run_motor(duty_2.get())

        if task_user.endProgram == True:
            motor_task_1.run_motor(0)
            motor_task_2.run_motor(0)
            break
    print('The program has been run for', task_user.runs, ' time(s).')
