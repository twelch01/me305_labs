# -*- coding: utf-8 -*-
"""
@file lab1.py
@brief The objective of this lab assignment is to use Python REPL to program a Nucleo L476 development board so that LED can blink in three different LED pulse patterns.
@details  The program consists of five states where State0 Initiates the program and welcome the user. State1 is Waiting for the user to press the blue user button to initiate LED pulse pattern 1. State2 is LED pulse pattern 1 (LED alternatively flashes high and low in an interval of 0.5 sec). State3 is the LED pulse pattern 2 (For every 10 sec, LED brightness changes in a sine-like way ). State4 is LED pulse pattern 3 (For every 1 sec, LED brightness linearly increases and drops to low)
            While the user is in State 4, the user can press the button again to go back to State 2.
            
            Video Link: https://youtube.com/shorts/YdeICLmLXqQ?feature=share
@image html Lab1-1.png

@author Sangmin Sung
@author Travis Welch
@Date September 29, 2021
"""

import time
import pyb
import math
import utime


def onButtonPressFCN(IRQ_src):
    """
    @brief This function is used as an interrupt callback when the user presses the button. 
    @details By pressing the button, a global varaiable, Button_Pressed, is initialized 
               to "True" to allow the system to break out of the while loops in state 2,3,
               and 4 and move on to the next state.
    @param IRQ_src is a reference to the hardware device, causing the interrupt to occur. 
    """
    global Button_Pressed 
    Button_Pressed = True
    
def update_STW(current_time):
    """
    @brief This function is used to generate the magnitude of LED brightness. 
    @details The number returned ranges from 0 to 100 due to the modulo operation.
            The number returned manipulates the magnitude of the LED brightness.
    @param current_time is a local variable that represents the current reference time. 
    """
    return 100*(current_time % 1.0)

def update_SQW(current_time):
    """
    @brief This function is used to generate the magnitude of LED brightness. 
    @details The number returned is either 0 or 100. If current_time is less than 0.5,
            'current_time % 1.0 < 0.5' returns True, which is equal to 1. If current_time
            is greater than or equal to 0.5, 'current_time % 1.0 < 0.5' returns True, which
            is equal to 1. The number is multiplied by 100 before it is returned.
    @param current_time is a local variable that represents the current reference time. 
    """
    return 100*(current_time % 1.0 < 0.5)

def update_SW(current_time):
    """
    @brief This function is used to generate the magnitude of LED brightness. 
    @details The number returned ranges from 0 to 100, depending on the value of current_time.
            The mathematical expression below ensures that LED flashes in a sine-like way.
    @param current_time is a local variable that represents the current reference time. 
    """
    return 100*(0.5*math.sin(math.pi/5*current_time)+0.5)
def update_timer():
    """
    @brief This function is used to initialize the stop time. 
    @details When the function is called, stop_time is initilized to the current time and is 
            represented as an ending time of each LED pattern in the current state.
    @param current_time is aematical expression  local variable that represents the current reference time. 
    """
    global stop_time
    stop_time = utime.ticks_ms()
    
def reset_timer():
    """
    @brief This function is used to initialize the start time. 
    @details When the function is called, stop_time is initilized to the current time and is 
            represented as a starting time of each LED pattern in the current state.
    @param current_time is aematical expression  local variable that represents the current reference time. 
    """
    global start_time
    start_time = utime.ticks_ms()
    


if __name__ == '__main__':
    ## @brief A variable that represents the current state in the program.
    #  @details This variable is used for the system to move on to different states,
    #            allowing the LED to flash in different pulse patterns.
    state = 0 
    ## @brief A variable that represents the number of cycles.
    #  @details This variable is used to count the number of cycles until the user
    #            terminates the program. 1 cycle = Going through state 2, 3, and 4.
    run = 0
    ## @brief A pin object for PC13.
    #  @details When the button is inactive, pin PC13 will read high.
    #            When the button is active, pin PC13 will read low.
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ## @brief A pin object for LED
    #  @details This variable is used to toggle LD2 (LED).
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    ## @brief A variable for interrupt callbacks
    #  @details This variable is used to associate the callback function with the pin
    #            by setting up an external interrupt. For each time the user presses the button,
    #            it will associate with the function "onButtonPressFCN".
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    while(True):
        try:
            if (state == 0): # Give the user brief information about the program
                print("running state 0. Welcome to our program. There are three LED pulse patterns that can be implemented by pressing blue user button.")
                state = 1
                pass
            elif (state == 1): # Prompt the user to press the button to initiate the LED pulse pattern 1.
                print("running state 1. Press blue user button to cycle through LED patterns.")
                state = 2
                Button_Pressed = False 
                while(Button_Pressed == False): # Until the user presses the button, the program keeps running state 1.
                    if(Button_Pressed == True):
                        pass
            elif (state == 2):
                print("running state 2")
                tim2 = pyb.Timer(2, freq=20000)
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
                Button_Pressed = False
                duration = -1 # duration is initilized to -1 to bypass the while loop condition.
                while(Button_Pressed == False): #Until the user presses the button, the program keeps running state 2.
                    ## @brief A variable that represents reference time in the current state in the program.
                    #  @details This variable is used to define the reference time with respect to each pattern in the current state,
                    #            allowing the LED to flash in different pulse patterns.
                    time = 0        
                    reset_timer()
                    while(time >= duration):
                        t2ch1.pulse_width_percent(update_SQW(time)) 
                        time += 0.00022 # this increment is chosen to ensure that LED goes HIGH/LOW every 0.5 sec.
                        if(Button_Pressed == True):
                            break 
                        if (time >= 1):
                            update_timer()
                            duration = stop_time-start_time
                    duration = -1
                state = 3
                pass

            elif (state == 3):
                print("running state 3")
                Button_Pressed = False
                while(Button_Pressed == False): #Until the user presses the button, the program keeps running state 3.
                    time = 0        
                    reset_timer()
                    while(time >= duration):
                        t2ch1.pulse_width_percent(update_SW(time)) 
                        time += 0.00018 # this increment is chosen to ensure that the period of the sine wave is roughly 10 seconds
                        if(Button_Pressed == True):
                            break 
                        if (time >= 10):
                            update_timer()
                            duration = stop_time-start_time
                    duration = -1
                state = 4
                pass
            
            elif (state == 4):
                print("running state 4")
                Button_Pressed = False
                while(Button_Pressed == False): #Until the user presses the button, the program keeps running state 4.
                    time = 0        
                    reset_timer()
                    while(time >= duration):
                        t2ch1.pulse_width_percent(update_STW(time)) 
                        time += 0.00022 # this increment is chosen to ensure that the triangle pattern cycles every 1 second.
                        if(Button_Pressed == True):
                            break 
                        if (time >= 1):
                            update_timer()
                            duration = stop_time-start_time
                    duration = -1
                state = 2 # Loop back to state 2.
                run += 1 # count the number of cycles
                pass

        except KeyboardInterrupt: # The user uses keyboard interrupt to terminate the program.
            break
            print("")
    print("end program ")
