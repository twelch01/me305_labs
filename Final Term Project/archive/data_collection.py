# -*- coding: utf-8 -*-
"""
Created on Sat Dec  4 18:16:24 2021

@author: trwel
"""
import utime
class Data_collection:
    def __init__ (self,period,ball_position,panel_angle,ball_velocity,panel_velocity):
        
        
        
        self.x = ball_position[0]
        self.y = ball_position[1]
        
        self.theta_x = panel_angle[0]
        self.theta_y = panel_angle[1]
    
        self.xdot = ball_velocity[0]
        self.ydot = ball_velocity[1]
        
        self.wx= panel_velocity[0]
        self.wy= panel_velocity[1]
        self.wz= panel_velocity[2]
        
        self.period = period
        
        
        ## @brief A class object used to instantiate the current time.
        #  @details This object is used to help set a current time object
        #           relative to the hardware based timer that can be used for
        #           data collection of values from a specified encoder.
        #
        self.currentTime = utime.ticks_ms()

        ## @brief A class object used to instantiate the next time.
        #  @details This object is used to help set a next time object
        #           relative to the current time plus the period used for
        #           data collection of values from a specified encoder.
        #
        self.nextTime = utime.ticks_ms() + period

        ## @brief A class object used to trigger the printing of data collection.
        #  @details This object is used to help trigger the action of printing
        #           out the data collected into putty window. It is intialized
        #           to false to then become true when the data has been fully
        #           collected at the specifed period and given length of time.
        #
        self.printData = False

        
    def run(self):
        self.currentTime = utime.ticks_ms()
        if(len(self.my_list_time) == 0):  # Initial step for the data collection
            self.my_list_time.append(0)
            self.initialTime = self.currentTime
            self.my_list_position.append(self.position[self.motor_index].get())
            self.my_list_delta.append(self.delta[self.motor_index].get())
            self.currentTime = utime.ticks_ms()
            self.nextTime = utime.ticks_ms() + self.period
            
            
        if (self.currentTime >= self.nextTime):
            self.my_list_time.append(self.currentTime-self.initialTime)
            self.my_list_position.append(self.position[self.motor_index].get())
            self.my_list_delta.append(self.delta[self.motor_index].get())
            self.storing_number += 1
            self.nextTime = utime.ticks_ms() + self.period
            print("The motor", self.motorNumber[self.motor_index],
                  "data has been collected for ", self.currentTime-self.initialTime, "milliseconds")
        if(int(30000/self.period) == self.storing_number):
            self.printData = True