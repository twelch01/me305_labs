# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file closedloop_6.py
@brief A driver file for enforcing the closedloop control of Ball Balanencer.
@details This driver file contains one function that will take in the necessary data to calculate a new duty cycle for the selected motor. 


@image html closedloop_final.jpg "Closed Loop Diagram" width=600px


@author Sangmin Sung
@author Travis Welch
@date October 25, 2021
"""

from ulab import numpy as np

class Closedloop:
    ''' 
    @brief Closedloop Class
    @details This closed loop driver file has a constructor and one update function 
    @details that takes in the current state vector composed of variables that 
    @details change and then calculates the next duty cycle of the motor. This file
    @details can be used for any number of motors, and will be used for the balancing 
    @details with two motors for this final term project.
    '''

    def __init__(self,ball_position,panel_angle,ball_velocity,panel_velocity,duty,K):
        ''' 
        @brief Constructs a closedloop object
        @details This constructor is used to accept current ball position, panel angle, ball velocity,panel angular velocity, motor duty, and controller gain K from shares.py to generate a new duty for motor 1 or motor 2.
        @param ball_position Depicts the current postion of the ball(x or y) from the resistive panel on top of the balancing plateform
        @param panel_angle Depicts the current angle of the plateform(theta x or theta y) from the Inertial Measurement Unit on the plateform
        @param ball_velocity Depicts the current velocity of the ball(x or y) from the resistive panel on top of the balancing plateform
        @param panel_velocity Depicts the current angular velocity of the plateform(theta x or theta y) from the Inertial Measurement Unit on the plateform
        @param duty Depicts the next set duty cycle for the electric motor specified to perform closed loop control on that motor.
        @param K Depicts the closed loop controller gain values for each of the variables within the state vector(Ball position, Plateform angle, Ball velocity, Panel angular velocity)
        '''
        
        ## @brief A current ball position object (x or y).
        #  @details Depicts the current postion of the ball(x or y) from the resistive panel on top of the balancing plateform
        # 
        self.ball_position = ball_position
        
        ## @brief A current plateform angle object (theta x or theta y).
        #  @details Depicts the current angle of the plateform(theta x or theta y) from the Inertial Measurement Unit on the plateform
        # 
        self.theta = panel_angle
        
        ## @brief A current ball velocity object (xdot or ydot).
        #  @details Depicts the current velocity of the ball(x or y) from the resistive panel on top of the balancing plateform
        # 
        self.ball_velocity = ball_velocity
        
        ## @brief A current plateform angular velocity object (thetadot x or thetadot y).
        #  @details Depicts the current angular velocity of the plateform(thetadot x or thetadot y) from the Inertial Measurement Unit on the plateform
        # 
        self.w = panel_velocity
        
        ## @brief A variable created to set the torque constant of the electric motor.
        #  @details Depicts the torque constant of the motor, used to calculate the next duty cycle of the motor. This constant allows the user to convert from the electric domain to the rotational domain.
        # 
        self.Kt = 13.9  #Nm/A
        
        ## @brief A variable created to set the armature resistance of the electric motor.
        #  @details Depicts the armature resistance of the motor, used to calculate the next duty cycle of the motor. 
        # 
        self.R = 2.21   #ohms
        
        ## @brief A variable created to set the voltage of the electric motor.
        #  @details Depicts the armature resistance of the motor, used to calculate the next duty cycle of the motor. 
        # 
        self.Vdc = 12   #volts
        
        ## @brief A matrix variable created to store all the controller gains.
        #  @details Depicts the closed loop controller gain values for each of the variables within the state vector(Ball position, Plateform angle, Ball velocity, Panel angular velocity)
        #
        self.K_num=K
        
        
        ## @brief A variable used to set the duty cycle of a specified motor.
        #  @details Depicts the next set duty cycle for the electric motor specified to perform closed loop control on that motor.
        #
        self.duty = duty
      
    def update(self):
        ''' 
        @brief Computes the new duty based on the current state vector.
        @details This function computes the new duty cycle needed by a specified motor in order to balance the ball. 
        @details It does so through matrix multiplication with the current state vector, controller gains, and the motor constant to convert torque to duty.
        '''
        ## @brief A variable used to calculate the constant
        #  @details This constant is used to convert motor torque to the needed duty cycle.
        #
        self.constant = (1000*self.R)/(4*self.Kt*self.Vdc)
        
        ## @brief A matrix variable used to store the state variable.
        #  @details Depicts a new state variable that holds the variables(Ball position, Plateform angle, Ball velocity, Panel angular velocity)
        #
        self.statevector = np.array([[self.ball_position.get()],[self.theta.get()],[self.ball_velocity.get()],[self.w.get()]])
     
        
        duty = self.constant*np.dot(-self.K_num.get(),self.statevector)
     
        self.duty.put(duty[0][0])

   