# -*- coding: utf-8 -*-
"""
@file main_lab5.py
@brief Main file for the lab 5 reading & writing to IMU BNO055.
@details The main will instanciate the IMU to master mode, and configure it as
         a object to be used in the IMU_driver file. The Calibration status will be 
         checked and then when finshed the Calibration Coefficents get stored. Then the 
         Euler angles and angular velocities will be printed from the IMU every 3 seconds.

File link:  https://bitbucket.org/twelch01/me305_labs/src/master/Lab%202/Lab%2022222/

@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""
import pyb
from pyb import I2C
import imu_driver
import utime


if __name__ == '__main__':  
 
    ## @brief A I2C variable used for the IMU driver.
    #  @details Sets the i2c in master configuration, in order to communicate to and from the IMU.
    #
    i2c = I2C(1,pyb.I2C.MASTER)   
    
    ## @brief A imu object used to access imu_driver file.
    #  @details The i2c variable is passed into the imu object to set the attributes needed to use the IMU_driver and the functions within the file.
    #
    imu = imu_driver.IMU_driver(i2c) 

    ## @brief Set the operation mode of the imu to nine degree of freedom mode.
    #  @details The 12 correlates to nine degree of freedom mode via the datasheet as 0b1100. This mode will trigger the imu to use the write slaves\
    #           to return the euler angles and angular velocities of the IMU.
    #
    imu.run_operation(12)    # Nine Degree of Freedom mode
    
    while(True):
        ## @brief A vairable used to define the current calibration status of the IMU.
        #  @details Once the IMU status returns a 0b11111111 binary number the calbration is complete and ready for use.
        #           The calibration coefficients will then be read and stored into a variable.
        #
        cal_status = imu.get_IMU_Cal_status()
        if (cal_status[0] != 0b11111111):
            print('Please move the IMU until binary number is all = 1, Current Status:', bin(cal_status[0]))
            utime.sleep(0.1)
        elif(cal_status[0] == 0b11111111):
            cal_Coef = imu.get_IMU_Cal_Coef()
            print('Fully Calibrated, current status', cal_Coef)
            break

    while(True):
        
        if(cal_status[0] != 0b11111111):
            imu.run_operation(0) # configuration mode
            print('Calibrated Coefficients Imported')
            imu.set_IMU_Cal_Coef(cal_Coef)
            imu.run_operation(12) # NDOF 

        
        heading,pitch,roll = imu.get_Euler()
        
        ## @brief One of the Euler angles individualized to Heading.
        #  @details Using the imu_driver file function get_euler to return the current heading, then converting it from byte to decimal number.
        #
        heading = imu.convert_byte_to_int(heading)
        
        ## @brief One of the Euler angles individualized to Pitch.
        #  @details Using the imu_driver file function get_euler to return the current Pitch, then converting it from byte to decimal number.
        #
        pitch = imu.convert_byte_to_int(pitch)
        
        ## @brief One of the Euler angles individualized to Roll.
        #  @details Using the imu_driver file function get_euler to return the current Roll, then converting it from byte to decimal number.
        #
        roll = imu.convert_byte_to_int(roll)
        
        wx,wy,wz = imu.get_Velocity()
        
        ## @brief The Angular Velocity of the IMU individualized to Omega X.
        #  @details Using the imu_driver file function get_Velocity to return the current Omega X, then converting it from byte to decimal number.
        #
        wx = imu.convert_byte_to_int(wx)
        
        ## @brief The Angular Velocity of the IMU individualized to Omega Y.
        #  @details Using the imu_driver file function get_Velocity to return the current Omega Y, then converting it from byte to decimal number.
        #
        wy = imu.convert_byte_to_int(wy)
        
        ## @brief The Angular Velocity of the IMU individualized to Omega Z.
        #  @details Using the imu_driver file function get_Velocity to return the current Omega Z, then converting it from byte to decimal number.
        #
        wz = imu.convert_byte_to_int(wz)
        
        print('____________________________________________________________')
        print('Positions:')
        print("Heading:", heading, "Pitch:", pitch, "Roll:", roll)
        print('____________________________________________________________')
        print('Angular velocities:')
        print("w_x:", wx, "w_y:", wy, "w_z:", wz)
        print('____________________________________________________________')

        utime.sleep(.001)