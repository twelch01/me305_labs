# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file task_controller.py
@brief A task file used to consistantly run the controller driver file.
@details This task file contains one function that will take in the reqiured 
@details data to update the controller with the current velocity of the motor 
@details and user inputed values.
@author Sangmin Sung
@author Travis Welch
@date October 25, 2021
"""
"""
@page page1 Test Results
@subsection subsection1 Plot 1
@image html lab4_plot.jpg "Lab 4 Data Plot" width=600px

"""
import closedloop

class Task_controller:
    ''' 
    @brief Task_controller Class
    @details This has a constructor and run_controller() function to call update() in closedloop.py at a regular interval.
    '''

    def __init__(self,delta,ref_delta,kp):
        ''' 
        @brief Constructs an closedloop object
        @details This constructor is used to instantiate the closedloop object from the main to this task_controller class.
        @param closedloop is object passed in from the main, that contains the nessary paramters needed to update the closedloop.py driver file.
        @param delta is the current angular velocity pulled from the encoder.
        @param ref_delta is the user input desired angular velocity of the motor.
        @param kp is the propotional gain defined by the user input.
        '''
        
        ## @brief A Share class object for the current angular velocity of the motor.
        #  @details This object is used to compute a new duty cycle.
        self.delta = delta
        ## @brief A Share class object for the user input desired angular velocity of the motor.
        #  @details This object is used to compute a new duty cycle.
        self.ref_delta = ref_delta
        ## @brief A Share class object for the user input desired propotional gain of the motor.
        #  @details This object is used to compute a new duty cycle.
        self.kp = kp
        ## @brief A closedloop object
        #  @details Closedloop is instantiated here to be able to construct the below functions with the correct updates for the correct motor.
        self.closedloop = closedloop.Closedloop(self.delta,self.ref_delta,self.kp)

    def run_controller(self):
        ''' 
        @brief Runs the closedloop update function with updated parameters.
        @details This run_controller function is used to run the function update() in closedloop.py at a regular interval for the desired motor.
        @return closedloop.update() that returns the duty cycle computed based on the most recent current angular velocity and desired angular velocity.
        '''
        return self.closedloop.update()

