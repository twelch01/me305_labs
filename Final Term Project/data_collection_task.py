# -*- coding: utf-8 -*-
"""
@file data_collection_task.py
@brief   A Data collection task the collects ball variables as a function of time.
@details This file is used to implement a simple data collection interface that is 
         triggered from the task user and then pulls and stores the data from the 
         resistive touch panel located on top of the plateform.
        
@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""
import utime
import array

class Data_Collection_Task:
    ''' 
    @brief Data_Collection_Task Class
    @details This has a constructor and run() function collect the data from 
             the resitive touch plate at the interval of the period.
    '''

    def __init__(self,x,y,xdot,ydot,printData,period,state):
        ''' 
        @brief Constructs x, y, xdot, ydot, printData, period, and state
        @details This constructor is used to set the input parameters to variables as intial conditions for the run function.
        @param x The current position of the ball in the x-diection
        @param y The current position of the ball in the y-diection
        @param xdot The current velocity of the ball in the x-diection
        @param ydot The current velocity of the ball in the y-diection
        @param printData A input argument used to denote when the data should be printed
        @param period The time allocated between data points
        @param state The current state of the user task
        '''
        
        ## @brief A current ball position variable in x-direction.
        #  @details Depicts the current postion of the ball in the x-direction from the resistive panel on top of the balancing plateform
        # 
        self.x = x

        ## @brief A current ball position variable in y-direction.
        #  @details Depicts the current postion of the ball in the y-direction from the resistive panel on top of the balancing plateform
        # 
        self.y = y
        
        ## @brief A current ball velocity variable in x-direction.
        #  @details Depicts the current velocity of the ball in the x-direction from the resistive panel on top of the balancing plateform
        # 
        self.xdot = xdot

        ## @brief A current ball velocity variable in y-direction.
        #  @details Depicts the current velocity of the ball in the y-direction from the resistive panel on top of the balancing plateform
        # 
        self.ydot = ydot
        
        ## @brief An array that represents a list of the data for time that are collected for 30 seconds upon g command.
        #  @details nothing is initilally stored in my_list to let the system know that the list does not contain
        #  @details any encoder data. This list will be cleared after all the data collected are printed.
        #
        self.my_list_time = array.array('d',[])
        
        ## @brief An array that represents a list of the data for ball position in x-direction that is collected for 30 seconds upon g command.
        #  @details nothing is initilally stored in my_list to let the system know that the list does not contain
        #  @details any position data. This list will be cleared after all the data collected are printed.
        #
        self.my_list_x = array.array('d',[])
        
        ## @brief An array that represents a list of the data for ball position in y-direction that is collected for 30 seconds upon g command.
        #  @details nothing is initilally stored in my_list to let the system know that the list does not contain
        #  @details any position data. This list will be cleared after all the data collected are printed.
        #
        self.my_list_y = array.array('d',[])
        
        ## @brief An array that represents a list of the data for ball velocity in x-direction that is collected for 30 seconds upon g command.
        #  @details nothing is initilally stored in my_list to let the system know that the list does not contain
        #  @details any velocity data. This list will be cleared after all the data collected are printed.
        #
        self.my_list_xdot = array.array('d',[])
        
        ## @brief An array that represents a list of the data for ball velocity in y-direction that is collected for 30 seconds upon g command.
        #  @details nothing is initilally stored in my_list to let the system know that the list does not contain
        #  @details any velocity data. This list will be cleared after all the data collected are printed.
        #
        self.my_list_ydot = array.array('d',[])
        
        ## @brief A class object used to instantiate the intial time.
        #  @details This object is used to help set a intial time object that can
        #           be used for data collection of values from a specified encoder.
        #
        self.initialTime = 0

        ## @brief A class object used to instantiate the current time.
        #  @details This object is used to help set a current time object
        #           relative to the hardware based timer that can be used for
        #           data collection of values from a specified encoder.
        #
        self.currentTime = utime.ticks_ms()

        ## @brief A class object used to instantiate the next time.
        #  @details This object is used to help set a next time object
        #           relative to the current time plus the period used for
        #           data collection of values from a specified encoder.
        #
        self.nextTime = utime.ticks_ms() + period
        
        ## @brief A class object used to trigger the printing of data collection.
        #  @details This object is used to help trigger the action of printing
        #           out the data collected into putty window. It is intialized
        #           to false to then become true when the data has been fully
        #           collected at the specifed period and given length of time.
        #
        self.printData = printData
        
        ## @brief A class object used to store the number of data points taken.
        #  @details This object is used to help store the number of data points
        #           taken in order to stop the data collection at the specified
        #           length of time.
        #
        self.storing_number = 0

        ## @brief A class object used to store the specified step size for data collection
        #  @details This object is used to help store the step size specified
        #           in the main for data collection.
        #
        self.period = period
        
        ## @brief A class object used to store the current state of task_user
        #  @details The current state of the user task is used in this file to change the state in the user when the data collection is done.
        #
        self.state = state
        
    def run(self):
       """
       @brief a function for running the data collection task
       @details This function gets triggered by the Task_user to begin data 
                collection of all the variables associated with the ball for 30 seconds.
                If the user ends the data collection prematurely, the date will automatically print,
                otherwise it will continue collecting data until the 30 seconds is done and then print.
       """
        
       self.currentTime = utime.ticks_ms()
       if(self.printData.get() == False):
           if(len(self.my_list_time) == 0):  # Initial step for the data collection
               self.my_list_time.append(0)
               self.initialTime = self.currentTime
               self.my_list_x.append(self.x.get())
               self.my_list_y.append(self.y.get())
               self.my_list_xdot.append(self.xdot.get())
               self.my_list_ydot.append(self.ydot.get())
               self.currentTime = utime.ticks_ms()
               self.nextTime = utime.ticks_ms() + self.period
           if (self.currentTime >= self.nextTime):
                self.my_list_time.append(self.currentTime-self.initialTime)
                self.my_list_x.append(self.x.get())
                self.my_list_y.append(self.y.get())
                self.my_list_xdot.append(self.xdot.get())
                self.my_list_ydot.append(self.ydot.get())
                self.storing_number += 1
                self.nextTime = utime.ticks_ms() + self.period
                print("The data has been collected for ", self.currentTime-self.initialTime, "milliseconds")
           if(int(30000/self.period) == self.storing_number):
                self.printData.put(True)
            
       if(self.printData.get() == True):
            print('time, Ball x, Ball y,Ball v in x-dir, Ball v in y-dir')
            # print out all the data stored in self.my_list
            for i in range(len(self.my_list_time)):
                print('{:}, {:}, {:}, {:}, {:}'.format(self.my_list_time[i], self.my_list_x[i], self.my_list_y[i],self.my_list_xdot[i], self.my_list_ydot[i]))
            del self.my_list_time
            del self.my_list_x
            del self.my_list_y
            del self.my_list_xdot
            del self.my_list_ydot
            self.my_list_time = array.array('d',[])
            self.my_list_x = array.array('d',[])
            self.my_list_y = array.array('d',[])
            self.my_list_xdot = array.array('d',[])
            self.my_list_ydot = array.array('d',[])
            self.printData.put(False)
            self.storing_number = 0
            self.state.put(0)
            



    