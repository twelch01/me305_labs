# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file imu_driver_6.py
@brief A driver file to read and write to the IMU BNO055 over I2C.
@details This file contains 6 function files to read and write to the IMU BNO055.
@details The driver handles direct communication with the IMU and returns calibration
         coeffiecents, calibration status, read Euler angle position, and read
         angular velocity of the BNO055.
@author Sangmin Sung
@author Travis Welch
@date November 2, 2021
"""

class IMU_driver:
    ''' 
    @brief IMU Drvier Class for the BNO055 board that contains a gyroscope, magnometer, and accelerometer.
    @details This has a constructor and 6 function files for direct communication with
             the BNO055 IMU board. The is class has the power to return calibration status
             of the IMU and overwrite calibration coefficients. Also has the power to return
             oreintation information of the board in the form of Euler angles and the angular 
             velocity in 3 dimensions.
    '''

    def __init__(self,i2c):
        ''' 
        @brief Constructs a I2C object and Initial conditions of parameters.
        @details This constructor is used to accept i2c object for instanciating the IMU in master mode 
                 allowing to pull data or send data to the IMU BNO055.
        @param i2c Is an input argument that instanciates the BNO055 in master mode.
        '''
        ## @brief Initialize the Angular velocity in the x-direction.
        #  @details Sets the Initial condition for angular velocity in the x-direction for the IMU BNO055.
        #
        self.w_x = 0
        
        ## @brief Initialize the Angular velocity in the y-direction.
        #  @detail  s Sets the Initial condition for angular velocity in the y-direction for the IMU BNO055.
        #
        self.w_y = 0
        
        ## @brief Initialize the Angular velocity in the z-direction.
        #  @details Sets the Initial condition for angular velocity in the z-direction for the IMU BNO055.
        #
        self.w_z = 0
        
        ## @brief Initialize i2c object for the imu_driver class.
        #  @details Initialize the input argument to a local i2c object to be used within this class.
        #
        self.i2c = i2c
        
        ## @brief Initialize Heading Euler angle.
        #  @details Sets the Initial condition for the Heading Euler angle for the IMU BNO055.
        #
        self.Heading = 0
        
        ## @brief Initialize Roll Euler angle.
        #  @details Sets the Initial condition for the Roll Euler angle for the IMU BNO055.
        #
        self.Roll = 0
        
        ## @brief Initialize Heading Euler angle.
        #  @details Sets the Initial condition for the Heading Euler angle for the IMU BNO055.
        #
        self.Pitch = 0
        
        ## @brief Initialize an empty variable for storing the Calibration Coefficents.
        #  @details Sets the Initial condition as an empty variable for placing the 22 calibration coefficients in for the IMU BNO055.
        #
        self.Calibration_Coef = ''
        
        
    def run_operation(self,Op_mode):
        ''' 
        @brief Function that writes to the IMU BNO055 to change the operation mode. 
        @details This function will be used to change the current operation mode of the IMU BNO055, 
                 the most common operation mode to be used is Nine Degree of Freedom mode, and configuration mode.
        @param Op_mode Input argument that defines the binary or decimal number that represents an operation mode detailed in the BNO055 datasheet.
        '''
        ## @brief Writes to the IMU BNO055 to change the operation mode.
        #  @details Inputs the desired operation mode for the IMU BNO055, and then sends it to the IMU through the I2C memory write command. Operation modes can be found on page 21 of the data sheet for the BNO055
        #
        self.i2c.mem_write(Op_mode,0x28,0x3D)
    
        
    def get_IMU_Cal_status(self):
        ''' 
        @brief Function that reads to the IMU BNO055 to return the calibration status.
        @details This function will be used obtain the current calibration status of the IMU BNO055, 
                 once calibration is complete, the status should read a binary number that is all 1's.
        @return Function will return the current calibration status of the IMU BNO055.(pg 67 and pg 48 in datasheet)
        '''
        ## @brief Reads the IMU BNO055 for the Current calibration status.
        #  @details Stores the calibration status with this variable. 
        #
        self.calibration_status = self.i2c.mem_read(1,0x28,0x35)
        
        return self.calibration_status
    
        
    def get_IMU_Cal_Coef(self):
        ''' 
        @brief Function that reads the IMU BNO055 to return the calibration coeffiecents.
        @details This function will be used obtain the current calibration coeffiecents of the IMU BNO055, 
                 once calibration is complete, the main should automatically store the Coeffiecents.
        @return Function will return the current Calibration Coefficients of the IMU BNO055.
        '''
        ## @brief Reads the IMU BNO055 for the current calibration coefficents.
        #  @details Stores the calibration Coeffiecients with this variable. 
        #
        self.Calibration_Coef = self.i2c.mem_read(22,0x28,0x55)
        return self.Calibration_Coef

    
    def set_IMU_Cal_Coef(self,Cali_coefficient):
        ''' 
        @brief Function that writes to the IMU BNO055 to overwrite the calibration coeffiecents.
        @details This function will be used overwrite the current calibration coeffiecents of the IMU BNO055, 
                 the main should automatically store the Coeffiecents. On page 48 of the datasheet, the operation mode needs to be in configuration mode to be able to implement this.
        '''
        self.i2c.mem_write(Cali_coefficient,0x28,0x55)
       
        
    def get_Euler(self):
        ''' 
        @brief Function that reads the IMU BNO055 to return the orientation of the BNO055 through Euler angles.
        @details This function will be used obtain the current orientation of the IMU BNO055, 
                 the way that it will do this is with Euler angles. The Euler angles the BNO055
                 will output is Heading,Pitch, and Roll. This information will get transfered from the
                 hardware to the main with this function.
        @return Function will return the current Euler angles indicating orientation of the IMU BNO055.
        '''
        ## @brief Reads the IMU BNO055 for the Euler angles and stores them.
        #  @details The Euler variable stores all the angles, including heading, pitch, and roll. 
        #
        Euler = self.i2c.mem_read(6,0x28,0x1A)
        self.Heading = (Euler[0] | Euler[1] << 8)
        self.Roll = (Euler[2] | Euler[3] << 8)
        self.Pitch = (Euler[4] | Euler[5] << 8)   
    
        return (self.Heading, self.Pitch, self.Roll)
    
    
    def get_Velocity(self):
        ''' 
        @brief Function that reads the IMU BNO055 to return the angular velocity of the BNO055.
        @details This function will be used obtain the current angular velocity of the IMU BNO055. The 
                 angular velocity of the system will return in all three orientations: X, Y, and Z.
        @return Function will return the current angular velocity of the IMU BNO055.
        '''
        ## @brief Reads the IMU BNO055 for the angular velocity.
        #  @details The w variable stores all the velocities, along the x, y, and the z direction. 
        #
        w = self.i2c.mem_read(6,0x28,0x14)
        self.w_x= (w[0] | w[1] << 8)
        self.w_y = (w[2] | w[3] << 8)
        self.w_z= (w[4] | w[5] << 8)
                
        return (self.w_x,self.w_y,self.w_z)
    
    def convert_byte_to_int (self,item):
        ''' 
        @brief Function that reads binary number and converts it to a decimal number.
        @details This function will be used obtain the Euler and angular velocity values and convert them to a decimal number.
        @param item Is used as a place holder, to allow any binary number needed to be converted to a decimal number.
        @return Function will return the input item to a decimal number.
        '''
        if item > 32767:
            item -= 65536
        item /= 16
        return item
        
        
        
        