# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file closedloop.py
@brief A driver file for enforcing the closedloop control on the motor
@details This driver file contains three functions that will take in the nessary data to calculate a new duty cycle for the selected motor. 


@image html closedloop_diagram.jpg "Closed Loop Diagram" width=600px


@author Sangmin Sung
@author Travis Welch
@date October 25, 2021
"""

from ulab import numpy as np

class Closedloop:
    ''' 
    @brief Closedloop Class
    @details This has a constructor and three functions to measure and enforce closedloop control on each motor.
    '''

    def __init__(self,ball_position,panel_angle,ball_velocity,panel_velocity):
        ''' 
        @brief Constructs a closedloop object
        @details This constructor is used to accept sat_limit(saturation limit) and Kp (proportional gain) from shares.py to generate a new duty for motor 1 or motor 2.
        @param sat_limit Input argument that represents the saturation limit, which is based on the hardware limitations of the motors.
        @param Kp Input argument that represents the user specified closedloop controller proportional gain.
        '''
        ## @brief A saturation limit object
        #  @details Sets the limits of the system based on the capabilities of the motor.
        #
        
        self.x = ball_position[0]
        self.y = ball_position[1]
        
        self.theta_x = panel_angle
        self.theta_y = panel_angle
    
        self.xdot = ball_velocity[0]
        self.ydot = ball_velocity[1]
        
        self.wx= panel_velocity[0]
        self.wy= panel_velocity[1]
        self.wz= panel_velocity[2]
        
        self.Kt = 13.9*0.001  #Nm/A
        self.R = 2.21   #ohms
        self.Vdc = 12   #volts
        
        self.A_num=np.array([0,0,1,0],[0,0,0,1],[-5.217,4.0111,0,0],[112.8871,64.8295,0,0])
        
        self.B_num=np.array([0],[0],[32.4991],[-703.2272])
        
        self.K_num=np.array([-0.3,-0.2,-0.05,-0.02])
        
        self.statevector_1 = np.array([self.x],[self.theta_y],[self.xdot],[self.wy])
        self.statevector_2 = np.array([self.y],[self.theta_x],[self.ydot],[self.wx])
        
        
        self.newstate_1 = []
        self.newstate_2 = []
        
        self.duty_1 = 0
        self.duty_2 = 0
        
        
        self.MM =np.dot(self.B_num,self.K_num)
        self.MM_sub= np.subtract(self.A_num,self.MM)
        
      
    def update(self):
        ''' 
        @brief Computes the new duty based on current angular velocity
        @details This update function is used to accept sat_limit(saturation limit) and Kp (proportional gain).
        @details The difference between the current angular velocity and the input velocity is multiplied with Kp to compute the new duty cycle of the motor.
        @return duty is the new duty cycle set by the closedloop controller interface of the system
        '''
      
        self.newstate_1 = np.dot(self.MM_sub,self.statevector_1)
        
        self.newstate_2 = np.dot(self.MM_sub.statevector_2)
        
        self.duty_1 = (100*self.R)/(4*self.Kt*self.Vdc)*(-1)*np.dot(self.K_num,self.newstate_1)
        
        self.duty_2 = (100*self.R)/(4*self.Kt*self.Vdc)*(-1)*np.dot(self.K_num,self.newstate_2)
        
        
        if abs(self.duty_1) > 100:
            print("The Calculated duty cycle excceeds sat_limit so the duty cycle is set to 0")
            print("Please change input values Kp or Desired Velocity")
            return 0
        else:
            return self.duty_1
        if abs(self.duty_2) > 100:
            print("The Calculated duty cycle excceeds sat_limit so the duty cycle is set to 0")
            print("Please change input values Kp or Desired Velocity")
            return 0
        else:
            return self.duty_2


   