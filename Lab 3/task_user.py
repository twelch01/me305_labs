# -*- coding: utf-8 -*-
"""
@file task_user3.py
@brief   A Task User file that delagtes the user inputed values
@details This file is used to implement a simple data collection interface, set user defined duty cycles, and to facilitate interaction
         with your encoder and motor objects. The lower Case letters represent motor/encoder 1 and the Upper letters represent motor/encoder 2.
         The user is given a short set of user commands as the followings:

         * z or Z : Zero the position of encoder 
         * p or P : Print out the position of encoder
         * d or D : Print out the delta from encoder
         * g or G : Collect encoder data for 30 seconds and print them as a comma seperated list
         * s or S : End data collection prematurely
         * e or E : end the program
         * c or C : Clear a extrernal interupt fault 
         * m or M : User set duty cycle for specified motor
         * k or K : User set proportional gain for the closedloop controller
         * v or V : User setpoint velocity for the closedloop controller 
         
@image html lab2_user_task.jpg "State Space Diagram" width=600px


@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""
import pyb
import utime
import array

# @brief A variable that represents the Vertual COM port.
#  @details This variable is used for understanding and taking in the user key commands.
#
serport = pyb.USB_VCP()

S0_INIT = 0  # Initial state
S1_ZERO_POSITION = 1  # "z"- Zero the position of the encoder 1
S2_PRINT_POSITION = 2  # "p"- print out the position of the encoder 1
S3_PRINT_DELTA = 3  # "d"- print out the delta for encoder 1
# "g"- Collect encoder 1 data for 30 seconds and print it to putty as a comma seperated list
S4_PRINT_30SEC = 4
S5_END = 5  # "e"- end data collection prematurely
S6_CLC_FAULT = 6  # "c"- clear the a fault condition triggered by the DRV8847
S7_USER_IN = 7  # "m"- user sets the duty cycle for motor 1
S8_USER_IN = 8  # "k"- user sets K_p for motor 1
S9_USER_IN = 9  # "v"- user sets set velocity for motor 1


# everything that follows will be inside the class
class Task_user:
    """
    @brief Task_user class
    @details This class has a constructor and run() to create a program that allows the user to use functions in encoder.py
    """
    # definition of constructor

    def __init__(self, period, motor_data1, motor_data2, enable):
        """
        @brief a constructor for Task_user class
        @details This constructor is used to accept two task encoder objects that are passed in from main_lab2.py 
        @details and initialize all the variables needed to successfully implement the data collection and
        @details to facilitate interaction with the encoder objects.
        @param period Input argument that represents the period in which data will be collected from each encoder.
        @param motor_data Input argument that represents the independent data that distinguish's each motor.
        @param enable Inoput argument that is used to enable a specified motor.
        """

        ## @brief A variable that represents the current state in the program
        #  @details This variable is used for the system to move on to different states,
        #  @details allowing the user to use desired commands.
        self.state = S0_INIT
        ## @brief An array that represents a list of the data for time that are collected for 30 seconds upon g command.
        #  @details nothing is initilally stored in my_list to let the system know that the list does not contain
        #  @details any encoder data. This list will be cleared after all the data collected are printed.
        self.my_list_time = array.array('d',[])
        ## @brief An array that represents a list of the data for encoder 1 shaft position that are collected for 30 seconds upon g command.
        #  @details nothing is initilally stored in my_list to let the system know that the list does not contain
        #  @details any encoder data. This list will be cleared after all the data collected are printed.
        self.my_list_position = array.array('d',[])

        ## @brief An array that represents a list of the data for encoder 1 shaft position that are collected for 30 seconds upon g command.
        #  @details nothing is initilally stored in my_list to let the system know that the list does not contain
        #  @details any encoder data. This list will be cleared after all the data collected are printed.
        self.my_list_delta = array.array('d',[])

        ## @brief A variable that counts the number of times the user uses the program.
        #  @details Every time the user presses e key command to end the program, this variable is incremented by 1
        self.runs = 0

        ## @brief A list that contains the Motor Numbers.
        #  @details This list is used to help the program distguish what
        #           key command should interface with a specified motor.
        #
        self.motorNumber = [motor_data1._buffer[0], motor_data2._buffer[0]]

        ## @brief A list that contains Share class objects of the current position of encoder 1 and encoder 2.
        #  @details This list is used to help store the encoder position to
        #           be used in other parts of the files.
        #
        self.position = [motor_data1._buffer[1], motor_data2._buffer[1]]

        ## @brief A list that contains Share class objects of the current velocity of encoder 1 and encoder 2.
        #  @details This list is used to help store the encoder velocity to
        #           be used in other parts of the files.
        #
        self.delta = [motor_data1._buffer[2], motor_data2._buffer[2]]

        ## @brief A list that contains Share class objects used to instantiate the zero postion of encoder.
        #  @details This list is used to help trigger the intialization of
        #           setting a particular encoder position to zero.
        #
        self.setZero = [motor_data1._buffer[3], motor_data2._buffer[3]]

        ## @brief A list that contains Share class objects used to instantiate the duty cycle of a motor.
        #  @details This list is used to help set a specified motor to a
        #           specified duty cycle.
        #
        self.duty = [motor_data1._buffer[4], motor_data2._buffer[4]]

        ## @brief A class object used to instantiate the intial time.
        #  @details This object is used to help set a intial time object that can
        #           be used for data collection of values from a specified encoder.
        #
        self.initialTime = 0

        ## @brief A class object used to instantiate the current time.
        #  @details This object is used to help set a current time object
        #           relative to the hardware based timer that can be used for
        #           data collection of values from a specified encoder.
        #
        self.currentTime = utime.ticks_ms()

        ## @brief A class object used to instantiate the next time.
        #  @details This object is used to help set a next time object
        #           relative to the current time plus the period used for
        #           data collection of values from a specified encoder.
        #
        self.nextTime = utime.ticks_ms() + period

        ## @brief A class object used to trigger the printing of data collection.
        #  @details This object is used to help trigger the action of printing
        #           out the data collected into putty window. It is intialized
        #           to false to then become true when the data has been fully
        #           collected at the specifed period and given length of time.
        #
        self.printData = False

        ## @brief A class object used to trigger ending the program at any time.
        #  @details This object is used to help trigger the action of ending
        #           the program at any point.
        #
        self.endProgram = False

        ## @brief A class object used to store the number of data points taken.
        #  @details This object is used to help store the number of data points
        #           taken in order to stop the data collection at the specified
        #           length of time.
        #
        self.storing_number = 0

        ## @brief A class object used to store the specified step size for data collection
        #  @details This object is used to help store the step size specified
        #           in the main for data collection.
        #
        self.period = period

        ## @brief A class object used to enable the specified motor out of sleep.low().
        #  @details This object is used to help trigger the intialization of
        #           bringing a specified motor out of sleep.low() mode.
        #
        self.enable = enable


        self.enable.put(False)

        ## @brief Instanciating the Key_input variable.
        #  @details Setting this variable to '' implies that it is intialized
        #           as an empty variable. When a user enters in a specific
        #           duty cycle the variable will generate a value.
        #
        self.Key_input = ''

        ## @brief Instanciating the Motor Index variable.
        #  @details Setting this variable to 0 will help the run the program,
        #           and when user enters an upper or lower case letter the pass
        #           variable will change to 1 or 2 to specify a specfic motor.
        #
        self.motor_index = 0

    def run(self):
        """
        @brief a function for running the program
        @details This function uses the Task_encoder_file class objects to implement all the functions created in encoder.py 
        @details whenever the user presses key commands. In addition to the functions in encoder.py, this function allows the user
        @details to implement a data collection interface and to print the data for encoder 1 and encoder 2 positions.
        """

        self.setZero[0].put(False)
        self.setZero[1].put(False)

        if(self.state == S7_USER_IN):  # print out the current delta values for encoder 1 and encoder 2
            if (serport.any()):
                char_input = serport.read(1).decode()
                if(char_input.isdigit()):  # Adds num to string if it is a digit
                    self.Key_input = self.Key_input + char_input
                    serport.write(char_input)

                # check if minus is first character, if it is then it will be added to string
                elif(char_input == '-'):
                    if(self.Key_input == ''):
                        self.Key_input = self.Key_input + char_input
                        serport.write(char_input)

                # backspace last key and changes num_st to appropriate length, as long as its not the first string
                elif(char_input == '\x7F'):
                    if(self.Key_input != ''):
                        self.Key_input = self.Key_input[:-1]
                        serport.write(char_input)

                elif(char_input == '.'):  # if the string does not already have decimal then adds to num_st
                    if(self.Key_input.find(char_input) == -1):
                        self.Key_input = self.Key_input + char_input
                        serport.write(char_input)

                # carriage return or new line for user pressing enter
                elif(char_input == '\n' or char_input == '\r'):
                    if(self.Key_input == ''):
                        self.Key_input = "0.0"
                    input_num = float(self.Key_input)
                    print('\n')
                    self.duty[self.motor_index].put(input_num)
                    self.Key_input = ''
                    self.state = S0_INIT  # returns to S0_INIT to continue running the program
        if(self.state == S4_PRINT_30SEC):
            self.currentTime = utime.ticks_ms()
            if(len(self.my_list_time) == 0):  # Initial step for the data collection
                self.my_list_time.append(0)
                self.initialTime = self.currentTime
                self.my_list_position.append(self.position[self.motor_index].get())
                self.my_list_delta.append(self.delta[self.motor_index].get())
                self.currentTime = utime.ticks_ms()
                self.nextTime = utime.ticks_ms() + self.period
            if (self.currentTime >= self.nextTime):
                self.my_list_time.append(self.currentTime-self.initialTime)
                self.my_list_position.append(
                    self.position[self.motor_index].get())
                self.my_list_delta.append(self.delta[self.motor_index].get())
                self.storing_number += 1
                self.nextTime = utime.ticks_ms() + self.period
                print("The motor", self.motorNumber[self.motor_index],
                      "data has been collected for ", self.currentTime-self.initialTime, "milliseconds")
            if(int(30000/self.period) == self.storing_number):
                self.printData = True
        if(self.state == S5_END or self.printData == True):
            print('time, encoder', self.motorNumber[self.motor_index],
                  ' position [rad], encoder ', self.motorNumber[self.motor_index], ' velocity [rad/s]')
            # print out all the data stored in self.my_list
            for i in range(len(self.my_list_time)):
                print('{:}, {:}, {:}'.format(self.my_list_time[i], self.my_list_position[i], self.my_list_delta[i]))
            del self.my_list_time
            del self.my_list_position
            del self.my_list_delta
            self.my_list_time = array.array('d',[])
            self.my_list_position = array.array('d',[])
            self.my_list_delta = array.array('d',[])
            self.state = S0_INIT  # returns to S0_INIT to continue running the program
            self.printData = False
            self.storing_number = 0
            self.nextTime = utime.ticks_ms() + self.period
        elif(serport.any()):  # waiting for the user key command.
            ## @brief A variable that represents the key command
            #  @details This variable is used to change the state in the program, allowing the user to use desired functions.
            char_in = serport.read(1)
            if(char_in.decode().isupper() == True):
                self.motor_index = 1
            else:
                self.motor_index = 0
            if(char_in.decode() == 'z' or char_in.decode() == 'Z'):
                self.state = S1_ZERO_POSITION
                pass
            elif(char_in.decode() == 'p' or char_in.decode() == 'P'):
                self.state = S2_PRINT_POSITION
                pass
            elif(char_in.decode() == 'd' or char_in.decode() == 'D'):
                self.state = S3_PRINT_DELTA
                pass
            elif(char_in.decode() == 'g' or char_in.decode() == 'G'):
                print('Collecting encoder ',
                      self.motorNumber[self.motor_index], ' data for 30 seconds and print data')
                self.state = S4_PRINT_30SEC
                pass
            elif(char_in.decode() == 'm' or char_in.decode() == 'M'):
                print('Please Enter Duty Cycle for Motor ',
                      self.motorNumber[self.motor_index], ':')
                self.state = S7_USER_IN
                pass
            elif(char_in.decode() == 's' or char_in.decode() == 'S'):
                self.state = S5_END
                self.printData1 = True
                print('ending data collection prematurely')
                pass
            elif(char_in.decode() == 'e' or char_in.decode() == 'E'):
                self.endProgram = True
                pass
            if(self.state == S1_ZERO_POSITION):  # set the shaft positions for both encoders to zero
                print('Zeroing the position of the encoder ',
                      self.motorNumber[self.motor_index])
                self.setZero[self.motor_index].put(True)
                self.state = S0_INIT  # returns to S0_INIT to continue running the program
                pass
            # print out the current shaft positions for encoder 1 and encoder 2
            if(self.state == S2_PRINT_POSITION):
                print('Printing out the position of the encoder ',
                      self.motorNumber[self.motor_index])
                print('Position', self.motorNumber[self.motor_index],
                      ': ', self.position[self.motor_index].get())
                self.state = S0_INIT  # returns to S0_INIT to continue running the program
                pass
            # print out the current delta values for encoder 1 and encoder 2
            if(self.state == S3_PRINT_DELTA):
                print('Printing out the encoder ',
                      self.motorNumber[self.motor_index], ' velocity')
                print('Encoder', self.motorNumber[self.motor_index],
                      ' velocity: ', self.delta[self.motor_index].get())
                self.state = S0_INIT  # returns to S0_INIT to continue running the program
                pass
            if(char_in.decode() == 'c' or char_in.decode() == 'C'):
                self.enable.put(True)
                self.duty[0].put(0)
                self.duty[1].put(0)
                pass
        self.runs += 1  # increase the run count by 1
