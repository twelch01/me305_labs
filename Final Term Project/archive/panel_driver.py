# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file 
@brief 
@details This file contains 6 function files to read and write to the IMU BNO055.

@author Sangmin Sung
@author Travis Welch
@date November 16, 2021
"""
import pyb, utime

class Panel_driver:
    ''' 
    @brief 
    @details 
    '''
 
    def __init__(self):
        ''' 
        @brief 
        @details 
        @param 
        '''
        
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        
        self.width = 100 ## y width of board
        self.length = 176 ## x length of board
        
        self.x_c = 0
        self.y_c = 0
        self.z_c = False
        
        self.xb = []
        self.yb = []
        
    def x_scan(self):
        ''' 
        @brief  
        @details 
        
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.IN)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
        self.xp.high()
        utime.sleep(0.000007)
        self.xm.low()
        
        adc = pyb.ADC(self.ym)
        adc_x = adc.read()
        
        self.xb = adc_x*(self.length/4095)-(self.length/2)
        return self.xb
        
    def y_scan(self):
        ''' 
        @brief 
        @details 
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.IN)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        self.yp.high()
        utime.sleep(0.000007)
        self.ym.low()
        
        adc = pyb.ADC(self.xm)
        adc_y = adc.read()
        
        self.yb = adc_y*(self.width/4095)-(self.width/2)
        return self.yb
        
            
    def z_scan(self):
        ''' 
        @brief 
        @details 
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
        self.yp.high()
        utime.sleep(0.000007)
        self.xm.low()
    
        adc = pyb.ADC(self.xp)
        adc_z1 = adc.read()
        if adc_z1 >= 5:
            self.z = True
        else:
            self.z = False
        return self.z
    
        
    def panel_filter(self,x,y):
        ''' 
        @brief 
        @details  
        '''
        if len(self.xb) == 5:
            sum_x = self.xb[0]+self.xb[1]+self.xb[2]+self.xb[3]+self.xb[4]
            sum_y = self.yb[0]+self.yb[1]+self.yb[2]+self.yb[3]+self.yb[4]
            self.xb = []
            self.yb = []
        else:
            self.xb.append(x)
            self.yb.append(y)
            
       
if __name__ == '__main__':  
    
    panel = Panel_driver()
    while(True):
        panel.panel_filter(panel.x_scan(),panel.y_scan())
        utime.sleep(0.1)
        print('X Position:', panel.x_scan(),'Y Position:', panel.y_scan(), "In contact?? (True/False):", panel.z_scan())
  