# -*- coding: utf-8 -*-
"""
Created on Sat Dec  4 15:43:46 2021
@file touch_panel_task.py
@brief A touch panel task file for calibrating the panel and saving/loading calibration gains in a .txt file
@details This file contains a function that saves or loads calibration gains from a .txt file and
@details a function that calibrates x and y using the loaded calibration gains and that stores calibrated x,y,and velocities in x and y.

@author Sangmin Sung
@author Travis Welch
@date October 25, 2021

"""

import panel_driver,os

class Touch_Panel_Task:
    ''' 
    @brief a class for touch_panel_task
    @details a class representing a panel task that saves or loads calibration gains and that stores x and y locations of contact point.
    '''
    def __init__(self,x,y,xdot,ydot):
        ''' 
        @brief a constructor
        @details a contructor that instantiates x, y, velocities, calibration check object, and panel driver class object.
        @param x is x location for contact point
        @param y is y location for contact point
        @param xdot is velocity in x
        @param ydot is velocity in y
        '''
        ## @brief x component of contact point
        #  @details x component of contact point with the origin at the center of the panel.
        self.x = x
        ## @brief y component of contact point
        #  @details y component of contact point with the origin at the center of the panel.
        self.y = y
        ## @brief x componenet of ball velocity
        #  @details x componenet of ball velocity after calibration.
        self.xdot = xdot
        ## @brief y componenet of ball velocity
        #  @details y componenet of ball velocity after calibration.
        self.ydot = ydot
        ## @brief a panel driver class object
        #  @details a panel driver class object that outputs x and y components of contact point.
        self.panel = panel_driver.Panel_driver(x,y)
        ## @brief A calibration checker object
        #  @details this object checks whether the calibration gains can be loaded from a .txt file. if they can be loaded, True, If not, False.
        self.calibration_exist = False
    def save_or_store_calibration(self):
        ''' 
        @brief a function that saves or loads calibration gains
        @details This function saves or loads calibration gains from a txt.file to calibrate in the run function.
        '''
        filename = "RT_cal_coeffs.txt"
        if filename in os.listdir():
            # File exists, read from it
            with open(filename, 'r') as f:
                # Read the first line of the file
                cal_data_string = f.readline()
                # Split the line into multiple strings and then convert each one to a float
                
                
                ## @brief a list object that contains calibration gains
                #  @details a list object that contains calibration gains from .txt file.
                self.cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                self.calibration_exist = True
        else:
            # File doesnt exist, calibrate manually and write the coefficients to the file
            with open(filename, 'w') as f:
                # Perform manual calibration
                self.panel.calibration()
                
                k_xx = self.panel.k_xx
                k_xy = self.panel.k_xy
                k_yx = self.panel.k_yx
                k_yy = self.panel.k_yy
                x_c = self.panel.x_c
                y_c = self.panel.y_c
                
                # Then, write the calibration coefficients to the file as a string. 
                # The example uses an f-string, but you can use string.format() 
                # if you prefer
                f.write(f"{k_xx}, {k_xy}, {k_yx}, {k_yy}, {x_c}, {y_c}\r\n")
                
    def run(self):
        ''' 
        @brief a function that runs the calibration.
        @details This function uses the loaded calibration gains to calibrate the x and y components of contact point
        @details and velocities in x and y directions. 
        '''
        if self.calibration_exist == True:
            (self.panel.k_xx,self.panel.k_xy,self.panel.k_yx,self.panel.k_yy,self.panel.x_c,self.panel.y_c) = self.cal_values
            self.calibration_exist = False
        self.panel.xy_scan()
        if (self.x.get(),self.y.get()) !=(4096, 4096):
            z = self.panel.z_scan()
            if(z == True):
                new_x, new_y  = self.panel.panel_filter()
                self.x.put(new_x)
                self.y.put(new_y)
            else:
                self.x.put(0)
                self.y.put(0)

            
        if self.panel.ts != 0:
              self.xdot.put(self.panel.vx_hat)
              self.ydot.put(self.panel.vy_hat)
        self.past_x = self.x.get()
        self.past_y = self.y.get()
    