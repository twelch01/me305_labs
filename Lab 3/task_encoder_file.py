# -*- coding: utf-8 -*-
"""
@file task_encoder_file.py
@brief A task file for calling update() function at a regular interval
@details This file is used to allow the system to keep using the update() function in encoder.py for every time value = "period".

@image html lab2_encoder_task.jpg "State Space Diagram" width=600px

@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""

import utime
import encoder


class Task_encoder:
    ''' 
    @brief Task_encoder Class
    @details This has a constructor and run() function to call update() in encoder.py at a regular interval
    '''

    def __init__(self,period,pin_number,position,delta,setZero):
        ''' 
        @brief Constructs an Task_encoder object.
        @details This constructor is used to accept period and pin number from main_lab2.py to create a Encoder Class object for encoder 1 and encoder 2.
        @param period parameter that represents the defined time step.
        @param pin_number paramter that distingishes each of the encoders.
        @param position parameter that represents the current position of the respective encoder.
        @param delta parameter that represents the current change in position with respect to time of the respective encoder.
        @param setZero parameter that is used to pass in if the user has selected the set encoder position to zero.
        '''
        ## @brief a variable that represents the interval at which the system calls update() function in encoder.py
        #  @details This variable is used to increment self.next_time by its stored value.
        self.period = period
        
        ## @brief a variable used for run() function
        #  @details This variable is used to control how frequent the system calls update() function in encoder.py
        self.next_time = utime.ticks_ms() + period
        
        ## @brief A variable that represents Encoder class object 
        #  @details This class object allows the system to have access to update() function
        self.encoder_object = encoder.Encoder(pin_number)
        
        ## @brief A variable that represents position of the encoder.
        #  @details This variable is instanciated for this class to set variable position equal to the input parameter position as IC for this class.
        self.position = position
        
        ## @brief a variable that represents the change in position of the encoder.
        #  @details This variable is used to get the angular velocity of the motor with respect to time.
        self.delta = delta
        
        ## @brief a variable that represents if the user defined intial position.
        #  @details This variable is used to check if the user has selected the set current position of the encoder to zero in the user interface.
        self.setZero = setZero
        
        
    def run(self):
        ''' 
        @brief a function that calls update() function in encoder.py
        @details for every "period" value, update() function is called to update shaft positions and delta for encoder 1 or encoder 2. 
        '''
        if(utime.ticks_ms() >= self.next_time):
            self.encoder_object.update()
            self.position.put(self.encoder_object.get_position())
            self.delta.put(self.encoder_object.get_delta())
            if (self.setZero.get() == True):
                self.encoder_object.set_position(0)
            self.next_time += self.period
 