# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file panel_driver.py
@brief A driver file for calibrating the panel and outputing x and y locations of contact point.
@details This file contains two functions for scanning x and y contact point locations, a calibration function
@details ,and a filtering function that outputs x and y calibrated locations of contact point.

@author Sangmin Sung
@author Travis Welch
@date November 16, 2021
"""
import pyb, utime
from ulab import numpy as np



class Panel_driver:
    ''' 
    @brief a class for panel
    @details a class representing a driver for panel that outputs calibration gains and x and y locations of contact point.
    '''
 
    def __init__(self,x,y,k_xx=1,k_xy=1,k_yx=1,k_yy=1,x_c=0,y_c=0):
        ''' 
        @brief a constructor
        @details a contructor that instantiates pyb pins, calibration gains, x, and y used for outputing contact locations.
        @param x is x location for contact point
        @param y is y location for contact point
        @param kxx is a calibration gain
        @param kxy is a calibration gain
        @param kyx is a calibration gain
        @param kyy is a calibration gain
        @param x_c is x location of center of panel
        @param y_c is y location of center of panel
        '''
        
        ## @brief A pin object for pyb
        #  @details a positive pin associated with the contact point in the horizontal direction.
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        ## @brief A pin object for pyb
        #  @details a negative pin associated with the contact point in the horizontal direction.
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        ## @brief A pin object for pyb
        #  @details a positive pin associated with the contact point in the vertical direction.
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        ## @brief A pin object for pyb
        #  @details a negative pin associated with the contact point in the vertical direction.
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        
        ## @brief width of panel board
        #  @details width of panel board that is necessary for calibration
        self.width = 100 
        ## @brief length of panel board
        #  @details length of panel board that is necessary for calibration
        self.length = 176 
        
        ## @brief x component of center of the panel
        #  @details x component of center of the panel measured with the origin at the corner of the panel.
        self.x_c = x_c
        ## @brief y component of center of the panel
        #  @details y component of center of the panel measured with the origin at the corner of the panel.
        self.y_c = y_c
        ## @brief contact checking variable
        #  @details This variable checks whether a thing is in contact with the panel or not. True means in contact, and False means not in contact.
        self.z_c = False
        
        ## @brief x component of contact point
        #  @details x component of contact point with the origin at the center of the panel.
        self.x = x
        ## @brief y component of contact point
        #  @details y component of contact point with the origin at the center of the panel.
        self.y = y
        ## @brief a gain for x 
        #  @details a gain for x from adc reading used to compute x component of contact point.
        self.k_xx=k_xx
        ## @brief a gain for y
        #  @details a gain for y from adc reading used to compute x component of contact point.
        self.k_xy=k_xy
        ## @brief a gain for x 
        #  @details a gain for x from adc reading used to compute y component of contact point.
        self.k_yx=k_yx
        ## @brief a gain for y
        #  @details a gain for y from adc reading used to compute y component of contact point.
        self.k_yy=k_yy
        
        ## @brief x componenet of a point at bottom left corner of the panel.
        #  @details x componenet of a point at bottom left corner of the panel before calibration.
        self.x_BotLeft = 0
        ## @brief y componenet of a point at bottom left corner of the panel.
        #  @details y componenet of a point at bottom left corner of the panel before calibration.
        self.y_BotLeft = 0
        ## @brief x componenet of a point at bottom right corner of the panel.
        #  @details x componenet of a point at bottom right corner of the panel before calibration.
        self.x_BotRight = 0
        ## @brief y componenet of a point at bottom right corner of the panel.
        #  @details y componenet of a point at bottom right corner of the panel before calibration.
        self.y_BotRight = 0
        ## @brief x componenet of a point at top left corner of the panel.
        #  @details x componenet of a point at top left corner of the panel before calibration.
        self.x_TopLeft = 0
        ## @brief y componenet of a point at top left corner of the panel.
        #  @details y componenet of a point at top left corner of the panel before calibration.
        self.y_TopLeft = 0
        ## @brief x componenet of a point at top right corner of the panel.
        #  @details x componenet of a point at top right corner of the panel before calibration.
        self.x_TopRight = 0
        ## @brief y componenet of a point at top right corner of the panel.
        #  @details y componenet of a point at top right corner of the panel before calibration.
        self.y_TopRight = 0
        
        ## @brief change in time 
        #  @details change in time between two consecutive contact point readings
        self.ts = 0
        
        ## @brief x componenet of ball contact point after calibration
        #  @details x componenet of a point at top right corner of the panel before calibration.
        self.x_hat = 0
        ## @brief x component of ball velocity 
        #  @details x componenet of ball velocity after calibration
        self.vx_hat = 0
        ## @brief y componenet of ball contact point after calibration
        #  @details y componenet of ball velocity after calibration.
        self.y_hat = 0
        ## @brief y componenet of ball velocity
        #  @details y componenet of ball velocity after calibration.
        self.vy_hat = 0
        
        ## @brief x from adc
        #  @details x reading from adc necessary for calibration
        self.adc_x = 0
        ## @brief y from adc
        #  @details y reading from adc necessary for calibration
        self.adc_y = 0
        ## @brief z from adc
        #  @details z reading from adc used to check whether a thing is in contact with the panel.
        self.adc_z = 0
        ## @brief initial time 
        #  @details intial time used to compute ts.
        self.initial_time = 0

    def xy_scan(self):
        ''' 
        @brief a function for x and y scanning
        @details a function that converts adc readings to x and y components of contact point before and after calibration.
        
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        utime.sleep_us(4)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.IN)
        utime.sleep_us(4)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        utime.sleep_us(4)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        utime.sleep_us(4)
        self.yp.high()
        utime.sleep_us(4)
        self.ym.low()
        
        ## @brief adc object
        #  @details adc object for y reading.
        adcy = pyb.ADC(self.xm)
        self.adc_y = adcy.read()
        
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        utime.sleep_us(4)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        utime.sleep_us(4)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.IN)
        utime.sleep_us(4)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
        utime.sleep_us(4)
        self.xp.high()
        utime.sleep_us(4)
        self.xm.low()
        utime.sleep_us(4)
                
        ## @brief adc object
        #  @details adc object for y reading.
        adcx = pyb.ADC(self.ym)
        self.adc_x = adcx.read()

    

        if(self.x_c == 0): #implying that it is uncalibrated
            self.x.put(self.adc_x)
            self.y.put(self.adc_y)
        else:
            self.x.put(self.adc_x*self.k_xx+self.adc_y*self.k_xy+self.x_c)
            self.y.put(self.adc_x*self.k_yx+self.adc_y*self.k_yy+self.y_c)
            

    def z_scan(self):
        ''' 
        @briefa a function for checking contact/ not contact.
        @details This function chekcs whether a thing is in contact with the panel or not.
        @return z is a boolean variable that is instantiated to True if in contact or to False if not in contact.
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
        self.yp.high()
        self.xm.low()
    
            
        ## @brief adc object
        #  @details adc object for contact checking reading.
        adcz = pyb.ADC(self.xp)
        self.adc_z1 = adcz.read()
        if self.adc_z1 >= 40:
            self.z = True
        else:
            self.z = False
        return self.z

        
    def calibration(self):
        ''' 
        @brief a function for calibration
        @details a function that uses four corner locations to calibrate the x and y outputs by finding calibration gains.
        
        '''
        print("Touch Bottom left corner")
        self.z_c = self.z_scan()
        while (self.z_c == False):
            self.z_c = self.z_scan()
            self.xy_scan()
            self.x_BotLeft = self.x.get()
            self.y_BotLeft = self.y.get()
        print(self.x_BotLeft, self.y_BotLeft)
        utime.sleep(0.5)
        self.z_c = self.z_scan()
        print("Touch Bottom Right corner")
        while (self.z_c  == False): 
            self.z_c = self.z_scan()
            self.xy_scan()
            self.x_BotRight = self.x.get()
            self.y_BotRight = self.y.get()
            
        print(self.x_BotRight, self.y_BotRight)
        utime.sleep(0.5)
        self.z_c = self.z_scan()
        print("Touch Top left corner")
        while (self.z_c == False):  
            self.z_c = self.z_scan()
            self.xy_scan()
            self.x_TopLeft = self.x.get()
            self.y_TopLeft = self.y.get()
            
        print(self.x_TopLeft, self.y_TopLeft)
        utime.sleep(0.5)
        self.z_c = self.z_scan()  
        print("Touch Top Right corner")
        while (self.z_c == False):  
            self.z_c = self.z_scan()
            self.xy_scan()
            self.x_TopRight = self.x.get()
            self.y_TopRight = self.y.get()
        print(self.x_TopRight, self.y_TopRight)
            
                
        ## @brief matrix for x and y
        #  @details matrix for x and y components of each corner of the panel. 
        self.matrix_b = np.array([[self.x_TopLeft,self.y_TopLeft,1],[self.x_TopRight,self.y_TopRight,1],
                                  [self.x_BotLeft,self.y_BotLeft,1],[self.x_BotRight,self.y_BotRight,1]])
                
        
        ## @brief matrix for theoretical x and y 
        #  @details matrix for theoretical x and y components of each corner of the panel.
        self.matrix_a = np.array([[-self.length/2, self.width/2], [self.length/2, self.width/2],
                                  [-self.length/2, -self.width/2], [self.length/2, -self.width/2]])
                
        ## @brief a transposed matrix
        #  @details a transposed matrix of matrix_b
        matrix_bTranspose = self.matrix_b.transpose()
                
        ## @brief a matrix multiplication output
        #  @details matrix multiplication of transposed matrix_b and matrix_b
        bTranspose_matrix_b = np.dot(matrix_bTranspose,self.matrix_b)
                
        ## @brief an inversed matrix of matrix_b
        #  @details an inversed matrix of transposed matrix_b found using a numpy function.
        inverse = np.linalg.inv(bTranspose_matrix_b)
                
        ## @brief a matrix multiplication output
        #  @details matrix multiplication of inversed transposed matrix_b and transposed matrix_b
        inverse_bTranspose = np.dot(inverse,matrix_bTranspose)
                
        ## @brief a matrix for calibration gains 
        #  @details a matrix for calibration gains found by the matrix mulitiplication.
        calibration_variables = np.dot(inverse_bTranspose,self.matrix_a)
        
        (self.k_xx, self.k_xy, self.k_yx, self.k_yy, self.x_c, self.y_c) = calibration_variables.flatten()
        
    
    
    def panel_filter(self):
        ''' 
        @brief a function for filtering panel outputs.
        @details a function used to compute the correct outputs of the x and y locations and velocities in x and y.
        @return x and y locations are returned in m
        '''
        if self.initial_time == 0:
            self.initial_time = utime.ticks_ms()
            self.x_hat = 0.85*self.x.get()
            self.y_hat = 0.85*self.y.get()
            
        else:
            self.current_time = utime.ticks_ms()
            self.ts = (self.current_time -self.initial_time)/1000
            self.x_hat = self.x_hat+0.85*(self.x.get()-self.x_hat)+self.ts*self.vx_hat
            self.y_hat = self.y_hat+0.85*(self.y.get()-self.y_hat)+self.ts*self.vy_hat
            self.vx_hat = self.vx_hat + 0.005/self.ts*(self.x.get()-self.x_hat)
            self.vy_hat = self.vy_hat + 0.005/self.ts*(self.y.get()-self.y_hat)
            self.initial_time = self.current_time
        return self.x_hat/1000,self.y_hat/1000
    
    