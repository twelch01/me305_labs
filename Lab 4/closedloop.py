# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file closedloop.py
@brief A driver file for enforcing the closedloop control on the motor
@details This driver file contains three functions that will take in the nessary data to calculate a new duty cycle for the selected motor. 
@details The data will be received through signals from STM32 processors that have hardwares that can read pulses from encoders and to count distance 
@details and direction of motion of the encoder shafts for the current delta.
@details The Kp and Set point velocity will be recieved via user imput, and the system will update dependent of the desired values. 

@image html closedloop_diagram.jpg "Closed Loop Diagram" width=600px


@author Sangmin Sung
@author Travis Welch
@date October 25, 2021
"""


class Closedloop:
    ''' 
    @brief Closedloop Class
    @details This has a constructor and three functions to measure and enforce closedloop control on each motor.
    '''

    def __init__(self, delta,input_delta,Kp):
        ''' 
        @brief Constructs a closedloop object
        @details This constructor is used to accept sat_limit(saturation limit) and Kp (proportional gain) from shares.py to generate a new duty for motor 1 or motor 2.
        @param sat_limit Input argument that represents the saturation limit, which is based on the hardware limitations of the motors.
        @param Kp Input argument that represents the user specified closedloop controller proportional gain.
        '''
        ## @brief A saturation limit object
        #  @details Sets the limits of the system based on the capabilities of the motor.
        #
        self.delta = delta
        
        self.input_delta = input_delta
        
        ## @brief A proportional gain object
        #  @details User defined proportional gain of the closedloop controller on the system.
        #
        self.Kp = Kp

    def update(self):
        ''' 
        @brief Computes the new duty based on current angular velocity
        @details This update function is used to accept sat_limit(saturation limit) and Kp (proportional gain).
        @details The difference between the current angular velocity and the input velocity is multiplied with Kp to compute the new duty cycle of the motor.
        @return duty is the new duty cycle set by the closedloop controller interface of the system
        '''
        
        self.duty = (self.input_delta.get()-float(self.delta.get()))*float(self.Kp.get())
        
        
        if abs(self.duty) > 100:
            print("The Calculated duty cycle excceeds sat_limit so the duty cycle is set to 0")
            print("Please change input values Kp or Desired Velocity")
            return 0
        else:
            return self.duty

    def get_Kp(self):
        ''' 
        @brief Pulls the Kp from the task_user via user input
        @details This get function is used to pull Kp (proportional gain) from the user interface, as it can change based on user input.
        @return Kp allows the Kp(proportional gain) to be available to this class.
        '''
        return self.Kp

    def set_Kp(self, Kp):
        ''' 
        @brief Sets the variable Kp to the desired Kp via user input.
        @details This set function is used to accept the current Kp(proportional gain) and store it to be used within this class.
        @param Kp is the current proportional gained pulled from the get function within the closedloop class.
        '''
        self.Kp = Kp
