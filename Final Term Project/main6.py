# -*- coding: utf-8 -*-
"""
Created on Sat Dec  4 16:50:51 2021
@file main6.py
@brief A main driver
@details This file is used to instantiate a imu task object, panel task object, motor task object, user task object.
@details The user has the power to keep running the program created in task_user6.py and to
@details quit whenever he/she wants. The number of times the program has been run will be displayed.

@image html ball_balancer_task_diagram.jpg "Ball Balancer Task Diagram" width=600px


@author Sangmin Sung
@author Travis Welch
@date October 25, 2021

"""
from ulab import numpy as np
import touch_panel_task,share,task_imu,task_controller_6,motor_task_6,DRV8847_6,task_user6, data_collection_task


if __name__ == '__main__': 
    ## @brief a share class object for eular angle Heading.
    #  @details Using the imu_driver file function get_euler to return the current heading, then converting it from byte to decimal number.
    #
    heading = share.Share()
    ## @brief a share class object for eular angle Pitch.
    #  @details Using the imu_driver file function get_euler to return the current Pitch, then converting it from byte to decimal number.
    #
    pitch = share.Share()
    ## @brief a share class object for eular angle Roll.
    #  @details Using the imu_driver file function get_euler to return the current Roll, then converting it from byte to decimal number.
    #
    roll = share.Share()
    ## @brief a share class object for angular Velocity of the IMU in x direction
    #  @details Using the imu_driver file function get_Velocity to return the current Omega X, then converting it from byte to decimal number.
    #
    wx = share.Share()
    ## @brief a share class object for angular Velocity of the IMU in y direction.
    #  @details Using the imu_driver file function get_Velocity to return the current Omega Y, then converting it from byte to decimal number.
    #
    wy = share.Share()
    ## @brief a share class object for angular Velocity of the IMU in z direction
    #  @details Using the imu_driver file function get_Velocity to return the current Omega Z, then converting it from byte to decimal number.
    #
    wz = share.Share()
    ## @brief an imu task object
    #  @details an imu task object that passes in eular angles and angular velocities share objects to store eular angles and angular velocities.
    #
    task_imu = task_imu.Task_imu(heading,pitch,roll,wx,wy,wz)
    
    task_imu.calibration()
    ## @brief a share class object
    #  @details a share class object that stores calibrated x location of contact point
    #
    x = share.Share()
    ## @brief a share class object
    #  @details a share class object that stores calibrated y location of contact point
    #
    y = share.Share()
    ## @brief a share class object
    #  @details a share class object that stores calibrated velocity in x direction
    #
    xdot = share.Share()
    ## @brief a share class object
    #  @details a share class object that stores calibrated velocity in y direction
    #
    ydot = share.Share()
    
    ## @brief a touch_panel_task class object
    #  @details a touch_panel_task class object that passes in x,y,xdot, and ydot share class objects for panel calibration.
    #
    task_panel = touch_panel_task.Touch_Panel_Task(x, y,xdot,ydot)
    task_panel.save_or_store_calibration()
    
    ## @brief a share class object
    #  @details a share class object that stores duty cycle for motor 1
    #
    duty_1 = share.Share()
    
    ## @brief a share class object
    #  @details a share class object that stores duty cycle for motor 2
    #
    duty_2 = share.Share()

    ## @brief a share class object
    #  @details a share class object that stores a list containing controller gains for motor 1
    #
    K_1 = share.Share()
    ## @brief a list object
    #  @details a list containing controller gains for motor 2
    #
    k_1 = np.array([[-.15,-43,-3,-.1]])
    K_1.put(k_1)
    
    ## @brief a share class object
    #  @details a share class object that stores a list containing controller gains for motor 2
    #
    K_2 = share.Share()
    ## @brief a list object
    #  @details a list containing controller gains for motor 2
    #
    k_2 = np.array([[-.15,-43,-3,-.1]])
    K_2.put(k_2)
    
    ## @brief a controller task class object
    #  @details a controller task class object that passes in x,pitch,xdot,wx,duty_1,K_1 share class objects to compute for duty cycle for motor 1.
    #
    task_controller_1 = task_controller_6.Task_controller(x,pitch,xdot,wx,duty_1,K_1)
    ## @brief a controller task class object
    #  @details a controller task class object that passes in y,roll,ydot,wy,duty_2,K_2 share class objects to compute for duty cycle for motor 1.
    #
    task_controller_2 = task_controller_6.Task_controller(y,roll,ydot,wy,duty_2,K_2)

    ## @brief A motor driver class object 
    #  @details The driver class is used to implement fault detection, fault condition clear, and instantiation of motor class object and to enable/disable motor.
    motor_drv = DRV8847_6.DRV8847()
    motor_drv.enable()
    ## @brief A class object that represents Motor class object for motor 1
    #  @details There are five parameters passed into this Motor Class object: motor driver time, two pin number, two driver pyb pin objects.
    motor_2 = motor_drv.motor(motor_drv.tim, 3, 4, motor_drv.IN_1, motor_drv.IN_2)
    ## @brief A class object that represents Motor class object for motor 1
    #  @details There are five parameters passed into this Motor Class object: motor driver time, two pin number, two driver pyb pin objects.
    motor_1 = motor_drv.motor(motor_drv.tim, 1, 2, motor_drv.IN_3, motor_drv.IN_4)
    
    ## @brief a period object
    #  @details a period object that defines the frequency of data collection
    #
    period = 100
    ## @brief a ball balancing activation object
    #  @details a ball balancing activation object that engages or disengage balancing.
    #
    Ball_Balanced = False
    
    ## @brief a share class object for enable
    #  @details a share class object that stores a boolean to activate clear-fault operation.
    #
    enable = share.Share()
    ## @brief a share class object for print activation
    #  @details a share class object that stores a boolean to activate printing operation
    #
    printData = share.Share()
    ## @brief a share class object for state
    #  @details a share class object that stores a current state of user interface.
    #
    state = share.Share()
    
    ## @brief A class object that represents Motor class object for motor 1
    #  @details There are five parameters passed into this Motor Class object: motor driver time, two pin number, two driver pyb pin objects.
    task_motor_1 = motor_task_6.Task_motor(motor_1,motor_drv,enable)

    ## @brief A class object that represents Motor class object for motor 1
    #  @details There are five parameters passed into this Motor Class object: motor driver time, two pin number, two driver pyb pin objects.
    task_motor_2 = motor_task_6.Task_motor(motor_2,motor_drv,enable)
    
    
    ## @brief a task user class object
    #  @details a task user class object that helps manipulate the state to engage/disengage balancing and to collect/print data.
    #
    task_user = task_user6.Task_user(enable,Ball_Balanced,printData,state)
    ## @brief a data collection task class object
    #  @details a data collection task class object that collects or prints data 
    #
    task_data = data_collection_task.Data_Collection_Task(x,y,xdot,ydot,printData,period,state)
    
    print('_________________________________________________________________________________')
    print('| Press one of the following letters to run a command on the Ball Balancer       |')
    print('|"b or B"- Engage balancing the plateform                                        |')
    print('|"q or Q"- Disengage balancing the plateform                                     |')
    print('|"g or G"- Start collecting data of the ball position & velocity                 |')
    print('|"c or C"- Clear a fault condition triggered by the DRV8847 for Encoder 1 and 2  |')
    print('|"s or S"- end data collection prematurely                                       |')
    print('|"e or E"- end the program                                                       |')
    print(' ________________________________________________________________________________')
    while(True):
        task_user.run()
        task_imu.run()
        task_panel.run()
        if (task_user.state.get() == 2):
            task_data.run()
        if(task_user.Ball_Balanced == True):
            task_controller_1.run()
            task_controller_2.run()
            task_motor_1.run(duty_1.get())
            task_motor_2.run(-duty_2.get())
        if(task_user.endProgram == True):
            print("Program has been run for ",task_user.runs)
            break
        print(duty_1.get())
        print(-duty_2.get())