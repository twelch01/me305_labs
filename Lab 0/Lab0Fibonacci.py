# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 12:47:38 2021
@file Lab0Fibonacci.py
@author: Travis Welch
"""

""" Fibonacci Sequence"""
'''@lab1_assignment.py
There must be a docstring at the beginning of a Python source file
with an \@file [filename] tag in it!
'''
# Function definition and intialization for main script
def fib (idx):
    '''
    @brief     This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
    Fibonacci number
    '''
    f0 = 0 #Definition of inital Fibonacci number at index 0
    f1 = 1 #Definition Fibonacci number at index 1
    for n in range(idx+1):
        if n==0:     #if user inputs the index 0 the loop will return fibonacci number as 0
            f_n = 0
        elif n==1:     #if user inputs the index 1 the loop will return fibonacci number as 1
            f_n = 1     
        elif n == 2:     # if user inputs the index greater than 1, the for 
                         # loop will return fibonacci number by inializing 
                         # f_n first in the elif and then continuing to else 
                         # until conditions are met for the for loop.
            f_n = f1+f0
            fn_minus_2= f1
        else:                   
            fn_minus_1=f_n
            f_n=fn_minus_1+fn_minus_2
            fn_minus_2 = fn_minus_1
        
    return f_n

# Main Script 
# if user inputs a negative value an error statement will appear and prompt the user to input another value
# At anytime if the user wants to quit, they can do so by inputing 'q' to break the loop of constantly inputing new indexs
if __name__ =='__main__':
    ispositive = False
    while ispositive == False:
        idx =input('Please enter postive value Index to find fibonacci number,Enter "q" to quit the program: ')
        if idx == 'q':
            break
        idx = int(idx)
        if idx > 0:
            ispositve = True
            print ('Fibonacci number at '
               'index {:} is {:}.'.format(idx,fib(idx)))
            continue
        print('ERROR invalid input')
