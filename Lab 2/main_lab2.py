# -*- coding: utf-8 -*-
"""
@file main_lab2.py
@brief A main driver
@details This file is used to pass two Task_encoder_file class objects into Task_user Class and to run the program created in task_user.py.
@details The user has the power to keep running the program and to quit whenever he/she wants. The number of times the program has been run will be displayed.

File link:  https://bitbucket.org/twelch01/me305_labs/src/master/Lab%202/Lab%2022222/

@image html lab2_task_diagram.jpg "Task Diagram" width=600px

@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""
import DRV8847
import task_encoder_file
import task_user 

if __name__ =='__main__':
    ## @brief A variable that represents Task_encoder_file class object for encoder 1  
    #  @details There are two parameters passed into this Task_encoderfile Class object: period and pin number.
    #  @details The period is defined as 10 miliseconds, and pin number is defined as 4. This Task_encoder_file class object
    #  @details will be passed into Task_user Class object later.
    task_encoder_1 = task_encoder_file.Task_encoder(10,4)
    
    ## @brief A variable that represents Task_encoder_file class object for encoder 2  
    #  @details There are two parameters passed into this Task_encoderfile Class object: period and pin number.
    #  @details The period is defined as 10 miliseconds, and pin number is defined as 3. This Task_encoder_file class object
    #  @details will be passed into Task_user Class object later.
    task_encoder_2 = task_encoder_file.Task_encoder(10,8)
    ## @brief A variable that represents Task_user class object 
    #  @details There are two Task_encoder_file class objects passed into this Task_encoderfile Class object. This class object
    #  @details will be used to implement a function called "run()" in Task_user.py to run the program. 
    task_user = task_user.Task_user(task_encoder_1,task_encoder_2)
    
    ## @brief A variable that helps the user either rerun the program or quits. 
    #  @details When this variable is any integer, the program will be rerun. When this variable is a character 'e', the program will not be rerun.
    #  @details Initially, this variable is initialized to 1 so that it can bypass the while loop condition. This variable will be changed by the user input.
    idx = 1
    motor_drv = DRV8847()
    motor_drv.enable()
    
    motor_1 = motor_drv.motor(1)
    motor_2 = motor_drv.motor(2)
    
    while(abs(idx) >= 0): # Unless idx is 'e', the user will keep running the program.
        task_user.run()
        print('The program has been run for', task_user.runs,' time(s).')
        idx = input ('Do you want to run the program? (enter e for no and any integer for yes): ')
        if idx == 'e':
            break
        else:
            idx = int(idx)
            continue
        
            


