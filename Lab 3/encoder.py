# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 17:22:25 2021
@file encoder.py
@brief A driver for reading from Quadrature Encoders
@details This driver file contains several functions that measure the movement of the encoder shafts and send out the data to the user. 
@details The data will be received through signals from STM32 processors that have hardwares that can read pulses from encoders and to count distance 
@details and direction of motion of the encoder shafts. 

@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""
import pyb
import math

class Encoder:
    ''' 
    @brief Encoder Class
    @details This has a constructor and four functions to measure the movement of the encoder shafts and to return the data
    '''
    def __init__(self,pin_number):
        ''' 
        @brief Constructs a Encoder object
        @details This constructor is used to accept pin number from main_lab2.py to generate measurements for either encoder 1 or encoder 2.
        @param pin_number is used to instanciate the intended encoder 1 or encoder 2.
        '''
        ## @brief A timer object 
        #  @details This timer object is set to pin number from main_lab2.py and has prescaler and period set to 0 and 65535 respectively.
        self.tim = pyb.Timer(pin_number,prescaler=0, period=65535) 
        
        if (pin_number == 4): 
            ## @brief A channel variable 
            #  @details A channel from the timer used to properly interface with quadratrue encoders.
            self.ch1_1 = self.tim.channel(1,mode = pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.B6)
            ## @brief A channel variable 
            #  @details A channel from the timer used to properly interface with quadratrue encoders.
            self.ch2_1 = self.tim.channel(2,mode = pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.B7)
            
        elif(pin_number == 8):
            ## @brief A channel variable 
            #  @details A channel from the timer used to properly interface with quadratrue encoders.
            self.ch1_1 = self.tim.channel(1,mode = pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.C6)
            ## @brief A channel variable 
            #  @details A channel from the timer used to properly interface with quadratrue encoders.
            self.ch2_1 = self.tim.channel(2,mode = pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.C7)
        
        ## @brief A variable that represents a datum for encoder shafts
        #  @details This variable is used to zero the encoder shaft position at a known starting angle.
        self.datum_position = 0
        
        ## @brief A variable that represents the difference in ticks
        #  @details This variable represents the difference in ticks from the timer and is used to update the current position.
        self.new_delta = 0
        
        ## @brief A variable that represents the largest possible number that the timer can hold.
        #  @details This variable ensures that the timer can hold the largest possible 16-bit number to make full use of 16-bit timer.
        self.cap = 65535

        ## @brief A variable that represents the current position
        #  @details This variable will be incremented by difference in ticks to determine the current position of encoder shafts. 
        self.position = 0
        
        ## @brief A variable that represents a tick from the timer
        #  @details This tick variable is used to find the delta for the position of encoder shafts.
        self.new_tick = 0
        
        ## @brief A variable that represents a tick from the timer
        #  @details This tick variable is used to find the delta for the position of encoder shafts.
        self.old_tick = 0
    
    def update(self):
        ''' 
        @brief A function that updates encoder position and delta
        @details This function updates encoder position and delta using the timer. This function also checks
        @details and corrects all overflow or underflow timer counts.
        '''
        self.new_tick = self.tim.counter()
        self.new_delta = self.new_tick - self.old_tick
        self.old_tick = self.new_tick
        if (abs(self.new_delta) > self.cap/2):
            if(self.new_delta < 0):
                self.new_delta += self.cap
            else:
                self.new_delta -= self.cap
        self.position += self.new_delta
    def get_position(self):
        ''' 
        @brief A function that returns encoder position shaft
        @details This function returns the position of the encoder shaft in radians
        '''
        return self.position*2*math.pi/4000

    def set_position(self, position):
        '''
        @brief A function that sets encoder position shaft
        @details This funciton sets the current position of the encoder shaft to "position"
        @param position represents the new position of the encoder shaft
        '''
        
        self.datum_position = position*2*math.pi/4000
        self.position = self.datum_position
        print('Setting current position to 0')

    def get_delta(self):
        '''
        @brief A function that returns encoder delta
        @details This function returns the change in position of the encoder shaft between the two most recent updates in radians per second
        '''
        return self.new_delta *2*math.pi/4000*100



