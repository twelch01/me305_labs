# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file DRV8847_6.py
@brief A driver file for enabling the rotation of the motors.
@details This driver file contains a total of 2 classes, 5 functions, and 2 constuctor that will take in the necessary data to control each motor.
@details The driver file will deal with motor faults as necessary, and enable/disable the motors at the right time.

@author Sangmin Sung
@author Travis Welch
@date October 25, 2021
"""

import pyb
import utime

class DRV8847:
    ''' 
        @brief A motor driver class for the DRV8847 from TI.
        @details Objects of this class can be used to configure the DRV8847
                 motor driver and to create one or moreobjects of the
                 Motor class which can be used to perform motor
                 control.
        
        Refer to the DRV8847 datasheet here:
        https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''

    def __init__ (self):
        ''' @brief Initializes and returns a DRV8847 object.
            @details This constructor is used to set up the pins and necessary data needed for the control of the motors.
        '''
        
        ## @brief A Sleep pin object
        #  @details Sets the pin for enabling & disabling the motors in push & pull format.
        #
        self.nSleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        
        ## @brief A Fault pin object
        #  @details Sets the pin for fault detection in push & pull format.
        #
        self.nFault = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.OUT_PP)
        
        ## @brief A Fault Detected object
        #  @details Sets external interupt of a fault detected and creates the path to the callback function.
        
        self.Fault_Detected = pyb.ExtInt(self.nFault, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        
        ## @brief A timer object
        #  @details Sets the specified timer of the motors and frequency.
        #
        self.tim= pyb.Timer(3,freq = 20000) 
        
        ## @brief A motor pin object
        #  @details Sets the pin to one side of motor 1 in push & pull format.
        #
        self.IN_1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
        
        ## @brief A motor pin object
        #  @details Sets the pin to one side of motor 1 in push & pull format.
        #
        self.IN_2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
        
        ## @brief A motor pin object
        #  @details Sets the pin to one side of motor 2 in push & pull format.
        #
        self.IN_3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
        
        ## @brief A motor pin object
        #  @details Sets the pin to one side of motor 2 in push & pull format.
        #
        self.IN_4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
        pass
    
    def enable (self):
        ''' @brief Brings the DRV8847 out of sleep mode.
            @details enable function first diables the fault_detected external
                     interupt, then turns the nSleep pin to high() along with
                     a short delay to turn the fault_detected external interupt
                     back on to check for faults in the system.
        '''
        
        self.Fault_Detected.disable()                
        self.nSleep.high()                      
        utime.sleep_us(25)                      
        self.Fault_Detected.enable()   
             
    def disable (self):
        ''' @brief Puts the DRV8847 in sleep mode.
            @details Sets the nSleep pin to low, in order to disable all motors
                     to ensure no damage is done to the system.
        '''
        
        self.nSleep.low()
        pass
    
    def fault_cb (self, IRQ_src):
        ''' @brief Callback function to run on fault condition.
            @details Sets the nSleep pin to low, in order to disable all motors
                     to ensure no damage is done to the system.
            @param IRQ_src The source of the interrupt request.
        '''
        self.nSleep.low()
        print('Fault has been detected, Press "c" to clear the fault')
        pass

    def motor (self,tim,ch_number_1,ch_number_2,pin_number_1,pin_number_2):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
            @details Defines the particular motor to the driver to specific
                     timer, channel numbers, and pin numbers.
            @param tim defines the motor specified timer
            @param ch_number_1 defines the motor specified channel 1
            @param ch_number_2 defines the motor specified channel 2
            @param pin_number_1 defines the motor specified pin 1
            @param pin_number_2 defines the motor specified pin 2
            @return An object of class Motor
        '''
        
        return Motor(tim,ch_number_1,ch_number_2,pin_number_1,pin_number_2)

class Motor:
    ''' @brief A motor class for one channel of the DRV8847.
        @details Objects of this class can be used to apply PWM to a given
        DC motor.
    '''

    def __init__(self,tim,ch_number_1,ch_number_2,pin_number_1,pin_number_2):
         ''' @brief Initializes and returns a motor object associated with the DRV8847.
             @details Objects of this class should not be instantiated
                      directly. Instead create a DRV8847 object and use
                      that to create Motor objects using the method
                      DRV8847.motor().
         '''
         
         ## @brief A duty cycle object
         #  @details Initializes the duty cycle of the motor to zero.
         #
         self.duty = 0
         
         ## @brief A channel 1 object
         #  @details Initializes the channel 1 of the speciefied motor with the correlated mode and pin number.
         #
         self.ch1_1 = tim.channel(ch_number_1,mode = pyb.Timer.PWM, pin = pin_number_1)
         
         ## @brief A channel 1 object
         #  @details Initializes the channel 1 of the speciefied motor with the correlated mode and pin number.
         #
         self.ch2_1 = tim.channel(ch_number_2,mode = pyb.Timer.PWM, pin = pin_number_2)
            
    def set_duty (self, duty):
        ''' @brief Set the PWM duty cycle for the motor channel.
            @details This method sets the duty cycle to be sent
                     to the motor to the given level. Positive values
                     cause effort in one direction, negative values
                     in the opposite direction.
            @param duty A signed number holding the duty cycle of the PWM signal sent to the motor
        '''
        self.duty = float(duty)
        
        if duty >= 0:
            if duty <= 100:
                self.ch1_1.pulse_width_percent(100)
                self.ch2_1.pulse_width_percent(100-duty)
            else:
                self.ch1_1.pulse_width_percent(100)
                self.ch2_1.pulse_width_percent(0)
        elif duty < 0:
            if duty >= -100:
                self.ch1_1.pulse_width_percent(100+duty)
                self.ch2_1.pulse_width_percent(100)
            else:
                self.ch1_1.pulse_width_percent(0)
                self.ch2_1.pulse_width_percent(100)
