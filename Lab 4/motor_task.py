# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file motor_task.py
@brief A task file used to consistantly run the motor DRV8847.py driver file.
@details This task file contains one function that will take in the reqiured 
@details data to update the motor driver with the correct duty cycle via task user.
@details If a motor fault occurs, then the motor task will deligate to motor driver to clear the fault via user input.

@author Sangmin Sung
@author Travis Welch
@date October 25, 2021
"""


class Task_motor:
    ''' 
    @brief Task_motor Class
    @details This has a constructor and run_motor() function to call set_duty() in DRV9947.py at a regular interval.
    '''

    def __init__(self,motor_object,motor_driver,enable):
        ''' 
        @brief Constructs motor_object, motor_driver, and enable objects.
        @details This constructor is used to set the input parameters to variables as intial conditions for this Task_motor.
        @param motor_object input argument that instanciates the user specified motor detials.
        @param motor_driver input argument that enables the motor driver intially.
        @param enable input argument that enables the motor driver via user input during a fualt occurance.
        '''
        ## @brief A motor object
        #  @details carries the details of specified motor pin numbers, timers, and channels.
        #
        self.motor_object = motor_object
        
        ## @brief A motor driver object
        #  @details defaults the motor to be in enabled on the intial running of program.
        #
        self.motor_driver = motor_driver
        
        ## @brief A enable object
        #  @details when a motor fault occurs, and the user clears it, enable will be the place holder that tells the driver file to enable the motor again.
        #
        self.enable = enable
        
    def run_motor(self,duty):
        ''' 
        @brief A function runs the motor via setting the duty cycle.
        @details This function will run the motor to a set duty cyle and if the user clears a fault, the motor will be enabled with the duty cyle set to zero.
        @param duty is the PMW[%] of the signal sent to engage the motor.
        '''
        self.motor_object.set_duty(duty)
        if(self.enable.get() == True):
            print('Fault has been cleared, all duties have been set to zero')
            self.motor_driver.enable()
            self.enable.put(False)



        