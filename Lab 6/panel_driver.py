# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file 
@brief 
@details This file contains 6 function files to read and write to the IMU BNO055.

@author Sangmin Sung
@author Travis Welch
@date November 16, 2021
"""
import pyb, utime,os,utime
from ulab import numpy as np



class Panel_driver:
    ''' 
    @brief 
    @details 
    '''
 
    def __init__(self,k_xx=1,k_xy=1,k_yx=1,k_yy=1,x_c=0,y_c=0):
        ''' 
        @brief 
        @details 
        @param 
        '''
        
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        
        self.width = 100 ## y width of board
        self.length = 176 ## x length of board
        
        self.x_c = x_c
        self.y_c = y_c
        self.z_c = False
        
        self.k_xx=k_xx
        self.k_xy=k_xy
        self.k_yx=k_yx
        self.k_yy=k_yy
        
        self.x_BotLeft = 0
        self.y_BotLeft = 0
        self.x_BotRight = 0
        self.y_BotRight = 0
        self.x_TopLeft = 0
        self.y_TopLeft = 0
        self.x_TopRight = 0
        self.y_TopRight = 0
        
        self.ts = 0
        self.x_hat = 0
        self.vx_hat = 0
        self.y_hat = 0
        self.vy_hat = 0
        
        self.adc_x = 0
        self.adc_y = 0
        self.adc_z = 0
    

    def xy_scan(self):
        ''' 
        @brief  
        @details 
        
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.IN)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
        self.xp.high()
        self.xm.low()
        
        adc = pyb.ADC(self.ym)
        self.adc_x = adc.read()

        
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.IN)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
        self.yp.high()
        self.ym.low()
        
        adc = pyb.ADC(self.xm)
        self.adc_y = adc.read()

        if(self.x_c == 0): #implying that it is uncalibrated
            self.x = self.adc_x
            self.y = self.adc_y
        else:
            self.x = self.adc_x*self.k_xx+self.adc_y*self.k_xy+self.x_c
            self.y = self.adc_x*self.k_yx+self.adc_y*self.k_yy+self.y_c
        
        return (self.x,self.y)
    
    def z_scan(self):
        ''' 
        @brief 
        @details 
        '''
        self.xp = pyb.Pin(pyb.Pin.cpu.A7, pyb.Pin.IN)
        self.xm = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(pyb.Pin.cpu.A6, pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
        self.yp.high()
        self.xm.low()
    
        adc = pyb.ADC(self.xp)
        self.adc_z1 = adc.read()
        if self.adc_z1 >= 40:
            self.z = True
        else:
            self.z = False
        return self.z

        
    def calibration(self):
        print("Touch Bottom left corner")
        self.z_c = self.z_scan()
        while (self.z_c == False):
            self.z_c = self.z_scan()
            self.x_BotLeft, self.y_BotLeft = self.xy_scan()
        print(self.x_BotLeft, self.y_BotLeft)
        utime.sleep(0.5)
        self.z_c = self.z_scan()
        print("Touch Bottom Right corner")
        while (self.z_c  == False): 
            self.z_c = self.z_scan()
            self.x_BotRight, self.y_BotRight = self.xy_scan()
        print(self.x_BotRight, self.y_BotRight)
        utime.sleep(0.5)
        self.z_c = self.z_scan()
        print("Touch Top left corner")
        while (self.z_c == False):  
            self.z_c = self.z_scan()
            self.x_TopLeft, self.y_TopLeft = self.xy_scan()
        print(self.x_TopLeft, self.y_TopLeft)
        utime.sleep(0.5)
        self.z_c = self.z_scan()  
        print("Touch Top Right corner")
        while (self.z_c == False):  
            self.z_c = self.z_scan()
            self.x_TopRight, self.y_TopRight = self.xy_scan()
        print(self.x_TopRight, self.y_TopRight)
            
        
        self.matrix_b = np.array([[self.x_TopLeft,self.y_TopLeft,1],[self.x_TopRight,self.y_TopRight,1],
                                  [self.x_BotLeft,self.y_BotLeft,1],[self.x_BotRight,self.y_BotRight,1]])
        self.matrix_a = np.array([[-self.length/2, self.width/2], [self.length/2, self.width/2],
                                  [-self.length/2, -self.width/2], [self.length/2, -self.width/2]])
        matrix_bTranspose = self.matrix_b.transpose()
        bTranspose_matrix_b = np.dot(matrix_bTranspose,self.matrix_b)
        inverse = np.linalg.inv(bTranspose_matrix_b)
        inverse_bTranspose = np.dot(inverse,matrix_bTranspose)
        calibration_variables = np.dot(inverse_bTranspose,self.matrix_a)
        
        (self.k_xx, self.k_xy, self.k_yx, self.k_yy, self.x_c, self.y_c) = calibration_variables.flatten()
        
    
    
    def panel_filter(self,x,y):
        ''' 
        @brief 
        @details  
        '''
        if self.ts == 0:
            self.initial_time = utime.ticks_ms()
            self.x_hat = 0.85*x
            self.y_hat = 0.85*y
        else:
            self.current_time = utime.ticks_ms()
            self.ts = self.current_time -self.initial_time
            self.vx_hat = self.vx_hat + 0.005/self.ts*(x-self.x_hat)
            self.vy_hat = self.vy_hat + 0.005/self.ts*(y-self.y_hat)
            self.x_hat = self.x_hat*0.85*(y-self.y_hat)+self.ts*self.vx_hat
            self.y_hat = self.y_hat*0.85*(y-self.y_hat)+self.ts*self.vy_hat
            self.initial_time = self.current_time
        return self.x_hat,self.y_hat
    
    
if __name__ == '__main__':  
    
    Uncalibrated_panel = Panel_driver()
    
    filename = "RT_cal_coeffs.txt"
    if filename in os.listdir():
        # File exists, read from it
        with open(filename, 'r') as f:
            # Read the first line of the file
            cal_data_string = f.readline()
            # Split the line into multiple strings and then convert each one to a float
            cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
    else:
        # File doesnt exist, calibrate manually and write the coefficients to the file
        with open(filename, 'w') as f:
            # Perform manual calibration
            Uncalibrated_panel.calibration()
            
            k_xx = Uncalibrated_panel.k_xx
            k_xy = Uncalibrated_panel.k_xy
            k_yx = Uncalibrated_panel.k_yx
            k_yy = Uncalibrated_panel.k_yy
            x_c = Uncalibrated_panel.x_c
            y_c = Uncalibrated_panel.y_c

            # Then, write the calibration coefficients to the file as a string. 
            # The example uses an f-string, but you can use string.format() 
            # if you prefer
            f.write(f"{k_xx}, {k_xy}, {k_yx}, {k_yy}, {x_c}, {y_c}\r\n")
    
    panel = Panel_driver(k_xx, k_xy, k_yx, k_yy, x_c, y_c)
    
    while(True):
        (x,y) = panel.xy_scan()
        if (x,y) !=(4096, 4096):
            new_x, new_y  = panel.panel_filter(x,y)
            print('x',new_x,'y',new_y)
  