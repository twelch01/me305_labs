# -*- coding: utf-8 -*-
"""
@file task_user.py
@brief A task file for implementing a data collection interface and interacting with encoder objects.
@details This file is used to implement a simple data collection interface and to facilitate interaction
         with your encoder objects. The user is given a short set of user commands as the followings:

         * z : Zero the position of encoder 1 and encoder 2
         * p : Print out the position of encoder 1 and encoder 2
         * d : Print out the delta for encoder 1 and encoder 2
         * g : Collect encoder 1 and encoder 2 data for 30 seconds and print them as a comma seperated list
         * s : End data collection prematurely
         * e : end the program
         
         
         
         
         
@image html lab2_user_task.jpg "State Space Diagram" width=600px


@author Sangmin Sung
@author Travis Welch
@date October 14, 2021
"""
import pyb
import utime
import gc
serport=pyb.USB_VCP()

S0_INIT             =0    ## Initial state
S1_ZERO_POSITION    =1    ## "z"- Zero the position of the encoder 1
S2_PRINT_POSITION   =2    ## "p"- print out the position of the encoder 1
S3_PRINT_DELTA      =3    ## "d"- print out the delta for encoder 1
S4_PRINT_30SEC      =4    ## "g"- Collect encoder 1 data for 30 seconds and print it to putty as a comma seperated list
S5_END              =5    ## "s"- end data collection prematurely

#everything that follows will be inside the class
class Task_user:
    """
    @brief Task_user class
    @details This class has a constructor and run() to create a program that allows the user to use functions in encoder.py
    """    
    #definition of constructor
    def __init__(self,encoder_1,encoder_2):
        """
        @brief a constructor for Task_user class
        @details This constructor is used to accept two task encoder objects that are passed in from main_lab2.py 
        @details and initialize all the variables needed to successfully implement the data collection and
        @details to facilitate interaction with the encoder objects.
        """
        ## @brief A variable that represents the current state in the program
        #  @details This variable is used for the system to move on to different states,
        #  @details allowing the user to use desired commands.
        self.state = 0
        ## @brief A variable that represents a list of the data for time that are collected for 30 seconds upon g command.
        #  @details 1 is initilally stored in my_list to let the system know that the list does not contain 
        #  @details any encoder data. This list will be cleared after all the data collected are printed.
        self.my_list_time = [1]
        ## @brief A variable that represents a list of the data for encoder 1 shaft position that are collected for 30 seconds upon g command.
        #  @details 1 is initilally stored in my_list to let the system know that the list does not contain 
        #  @details any encoder data. This list will be cleared after all the data collected are printed.
        self.my_list_position1 = [1]
        ## @brief A variable that represents a list of the data for encoder 2 shaft position that are collected for 30 seconds upon g command.
        #  @details 1 is initilally stored in my_list to let the system know that the list does not contain 
        #  @details any encoder data. This list will be cleared after all the data collected are printed.
        self.my_list_position2 = [1]
        ## @brief A variable that counts the number of times the user uses the program.
        #  @details Every time the user presses e key command to end the program, this variable is incremented by 1
        self.runs = 0
        ## @brief A variable that represents Task_encoder_file class object for encoder 1  
        #  @details This variable is used as a class object to allow the user to have access to all the 
        #  @details functions that are defined in encoder.py
        self.encoder_1 = encoder_1
        ## @brief A variable that represents Task_encoder_file class object for encoder 2  
        #  @details This variable is used as a class object to allow the user to have access to all the 
        #  @details functions that are defined in encoder.py 
        self.encoder_2 = encoder_2
    def run(self):
        
        """
        @brief a function for running the program
        @details This function uses the Task_encoder_file class objects to implement all the functions created in encoder.py 
        @details whenever the user presses key commands. In addition to the functions in encoder.py, this function allows the user
        @details to implement a data collection interface and to print the data for encoder 1 and encoder 2 positions.
        """
        
        print('Press one of the following letters to run a command on Encoder:') 
        print('"z"- Zero the position of the encoder')
        print('"p"- print out the position of the encoder')
        print('"d"- print out the delta for encoder')
        print('"g"- Collect encoder data for 30 seconds and print data')
        print('"s"- end data collection prematurely')
        print('"e"- end the program')
        while(self.state == S0_INIT): # unless the user presses key command "e" to end the program, the program will continue running by looping back to state = S0_INIT.
            self.encoder_1.run() # update the encoder 1 shaft position
            self.encoder_2.run() # update the encoder 2 shaft position          
            if(serport.any()): # waiting for the user key command.
                ## @brief A variable that represents the key command
                #  @details This variable is used to change the state in the program, allowing the user to use desired functions.
                char_in = serport.read(1)
                if(char_in.decode() == 'z'):
                    self.state = S1_ZERO_POSITION
                    pass
                elif(char_in.decode() == 'p'):
                    self.state = S2_PRINT_POSITION
                    pass
                elif(char_in.decode() == 'd'):
                    self.state = S3_PRINT_DELTA
                    pass
                elif(char_in.decode() == 'g'):
                    self.state = S4_PRINT_30SEC
                    pass
                elif(char_in.decode() == 'e'):
                    break 
                if(self.state == S1_ZERO_POSITION): # set the shaft positions for both encoders to zero
                    print('Zeroing the position of the encoder 1')
                    self.encoder_1.encoder_object.set_position(0)
                    print('Zeroing the position of the encoder 2')
                    self.encoder_2.encoder_object.set_position(0)
                    self.state = S0_INIT # returns to S0_INIT to continue running the program
                    pass
                if(self.state == S2_PRINT_POSITION): # print out the current shaft positions for encoder 1 and encoder 2
                    print('Printing out the position of the encoder 1 and encoder 2')
                    print('Position1: ', self.encoder_1.encoder_object.get_position())
                    print('Position2: ', self.encoder_2.encoder_object.get_position())
                    self.state = S0_INIT # returns to S0_INIT to continue running the program
                    pass
                if(self.state == S3_PRINT_DELTA): # print out the current delta values for encoder 1 and encoder 2
                    print('Printing out the delta for encoder 1 and encoder 2')
                    print('delta1: ',self.encoder_1.encoder_object.get_delta())
                    print('delta2: ',self.encoder_2.encoder_object.get_delta())
                    self.state = S0_INIT # returns to S0_INIT to continue running the program
                    pass
                if(self.state == S4_PRINT_30SEC): # collecting the data for encoder shaft positions and printing out the data.
                    print('Collecting encoder data for 30 seconds and print data')
                    ## @brief A variable that represents the initial time at which the data collection starts.
                    #  @details This variable is stored with a current time value generated by utime at the start of the data collection.
                    initial_time = utime.ticks_ms()
                    ## @brief A variable that determines how frequent the system collects and stores the data.
                    #  @details The variable is initialized to 100, allowing the system to collect the data every 100 miliseconds.
                    storing_time = 100
                    while(True):
                        self.encoder_1.run() # keep updating the encoder 1 position for data collection.
                        self.encoder_2.run() # keep updating the encoder 2 position for data collection.
                        if(self.my_list_time[0] == 1 and self.my_list_position2[0] == 1 and self.my_list_position2[0] == 1): # Initial step for the data collection
                            ## @brief A variable that represents encoder 1 position at a certain time 
                            #  @details This variable will be stored in the list "data_storage"
                            position1 = self.encoder_1.encoder_object.get_position()
                            ## @brief A variable that represents encoder 2 position at a certain time 
                            #  @details This variable will be stored in the list "data_storage"
                            position2 = self.encoder_2.encoder_object.get_position()
                            
                            reference_time = 0
                            
                            self.my_list_time[0] = reference_time
                            self.my_list_position1[0] = position1
                            self.my_list_position2[0] = position2
                        if(serport.any()): # allow the user to end the program whenever he/she wants by pressing "s".
                            char_in = serport.read(1)
                            if(char_in.decode() == 's'):
                                self.state = S5_END
                                print('ending data collection prematurely')
                                break
                        ## @brief A variable that represents the current reference time during the data collection.
                        #  @details This reference time variable is initially 0 and keeps changing until it reaches 30000 which is 30 seconds.
                        reference_time = utime.ticks_ms() - initial_time #
                        if(reference_time <= 30010): # the data collection continues for 30 seconds.
                            if (reference_time > storing_time):
                                self.my_list_time.append(reference_time)
                                position1 = self.encoder_1.encoder_object.get_position()
                                self.my_list_position1.append(position1)
                                position2 = self.encoder_2.encoder_object.get_position()
                                self.my_list_position2.append(position2)
                                storing_time +=100 # for every 100 miliseconds, one data is stored in the list "self.my_list"
                        else: # after 30 seconds, the data collection ends.
                            break
                    print('time, encoder 1 position, encoder 2 position')
                    for i in range(len(self.my_list_time)): # print out all the data stored in self.my_list
                        print('{:}, {:}, {:}'.format(self.my_list_time[i],self.my_list_position1[i],self.my_list_position2[i]))
                    self.my_list_time.clear()
                    self.my_list_position1.clear()
                    self.my_list_position2.clear()
                    self.my_list_time = [1]
                    self.my_list_position1 = [1]
                    self.my_list_position2 = [1]
                    self.state = S0_INIT # returns to S0_INIT to continue running the program
        self.runs+=1 # increase the run count by 1
                
                
                
                
                
                
          