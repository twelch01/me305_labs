# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 12:22:25 2021
@file task_controller_6.py
@brief A task file for distributing the closedloop control of Ball Balanencer.
@details This file is used as a means for passing variables from this task to other tasks and the closedloop driver file
@details that computes the next duty cycle for each motor on the ball ballancer. This file consists of a task_controller Class
@details with a constructor and a run function that continuely updates the closedloop driver with current statevector.

@author Sangmin Sung
@author Travis Welch
@date October 25, 2021
"""
import closedloop_6

class Task_controller:
    ''' 
    @brief Task_controller Class
    @details This controller task file has a constructor and one update function 
    @details that takes in the current state vector composed of variables that 
    @details change and then deligates them to the closedloop driver. This file
    @details can be used for any number of motors, and will be used for the balancing 
    @details with two motors for this final term project.
    '''
        
    def __init__(self,ball_position,panel_angle,ball_velocity,panel_velocity,duty,K):
        ''' 
        @brief Constructs an closedloop object
        @details This constructor is used to instantiate the closedloop object from the main to this task_controller class.
        @param ball_position Depicts the current postion of the ball(x or y) from the resistive panel on top of the balancing plateform
        @param panel_angle Depicts the current angle of the plateform(theta x or theta y) from the Inertial Measurement Unit on the plateform
        @param ball_velocity Depicts the current velocity of the ball(x or y) from the resistive panel on top of the balancing plateform
        @param panel_velocity Depicts the current angular velocity of the plateform(theta x or theta y) from the Inertial Measurement Unit on the plateform
        @param duty Depicts the next set duty cycle for the electric motor specified to perform closed loop control on that motor.
        @param K Depicts the closed loop controller gain values for each of the variables within the state vector(Ball position, Plateform angle, Ball velocity, Panel angular velocity)
        '''
        
        ## @brief A matrix variable created to store all the controller gains.
        #  @details Depicts the closed loop controller gain values for each of the variables within the state vector(Ball position, Plateform angle, Ball velocity, Panel angular velocity)
        #
        self.K = K
        
        ## @brief A current ball position object (x or y).
        #  @details Depicts the current postion of the ball(x or y) from the resistive panel on top of the balancing plateform
        # 
        self.ball_position = ball_position
        
        ## @brief A current plateform angle object (theta x or theta y).
        #  @details Depicts the current angle of the plateform(theta x or theta y) from the Inertial Measurement Unit on the plateform
        # 
        self.panel_angle = panel_angle
        
        ## @brief A current ball velocity object (xdot or ydot).
        #  @details Depicts the current velocity of the ball(x or y) from the resistive panel on top of the balancing plateform
        # 
        self.ball_velocity = ball_velocity
        
        ## @brief A current plateform angular velocity object (thetadot x or thetadot y).
        #  @details Depicts the current angular velocity of the plateform(thetadot x or thetadot y) from the Inertial Measurement Unit on the plateform
        # 
        self.panel_velocity = panel_velocity
        
        ## @brief A variable used to set the duty cycle of a specified motor.
        #  @details Depicts the next set duty cycle for the electric motor specified to perform closed loop control on that motor.
        #
        self.duty = duty
        
        ## @brief A closedloop object
        #  @details Closedloop is instantiated here to be able to construct the below functions with the correct updates for the correct motor.
        #
        self.closedloop = closedloop_6.Closedloop(self.ball_position,self.panel_angle,self.ball_velocity,self.panel_velocity,self.duty,self.K)

    def run(self):
        ''' 
        @brief Runs the closedloop update function with updated parameters.
        @details This run_controller function is used to run the function update() in closedloop_6.py at a regular interval for the desired motor.
      
        '''
        self.closedloop.update()